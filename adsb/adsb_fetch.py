#!/usr/bin/env python3

import os
import sys
import argparse
import signal
import requests
from requests.adapters import HTTPAdapter, Retry
from typing import List
from rich.progress import track
from bs4 import BeautifulSoup

s = requests.Session()

retries = Retry(total=10,
                backoff_factor=0.2,
                status_forcelist=[ 500, 502, 503, 504 ])

s.mount('https://', HTTPAdapter(max_retries=retries))

year = ""
month = ""
day = ""

parser = argparse.ArgumentParser()
parser.add_argument("year", type=int)
parser.add_argument("month", type=int)
parser.add_argument("day", type=int)
parser.add_argument("-c", "--check", action="store_true")
parser.add_argument("-d", "--data", default="../data/adsb/")
args = parser.parse_args()

year = str(args.year)
if args.month < 10:
    month = f"0{args.month}"
else:
    month = str(args.month)
if args.day < 10:
    day = f"0{args.day}"
else:
    day = str(args.day)

date_string = f"{year}/{month}/{day}"
print(f"ADSB Data for {date_string}")

root_url = f"https://samples.adsbexchange.com/readsb-hist/{date_string}/"

data_root = args.data


def get_file_list(date_string: str) -> List[str]:
    adsb_files = []

    list_response = requests.get(f"https://samples.adsbexchange.com/readsb-hist/{date_string}/")
    soup = BeautifulSoup(list_response.text, "html.parser")
    rows = soup.html.body.table.tbody.find_all("td")

    for row in rows:
        if row.span:
            if row.span.text.endswith(".json.gz"):
                adsb_files.append(row.span.text)

    #print(len(adsb_files))
    return adsb_files


def check_data(data_root: str, date_string: str):
    adsb_files = get_file_list(date_string)

    have_count = 0
    missing_count = 0

    for adsb_filename in adsb_files:
        output_filename = f"{data_root}/{date_string}/{adsb_filename.replace('.gz', '')}"

        if not os.path.exists(output_filename):
            missing_count = missing_count + 1
        else:
            have_count = have_count + 1

    print(f"{have_count / missing_count * 100.0:.2f}% complete")
    print(f"have: {have_count}")
    print(f"missing: {missing_count}")


def download_data(data_root: str, date_string: str):
    os.makedirs(f"{data_root}/{date_string}", exist_ok=True)

    adsb_files = get_file_list(date_string)

    for adsb_filename in track(adsb_files, description="Aquiring..."):
        input_filename = root_url + adsb_filename
        output_filename = f"{data_root}/{date_string}/{adsb_filename.replace('.gz', '')}"

        if not os.path.exists(output_filename):
            #print(file_name)
            f = s.get(input_filename)
            #print(f.text)

            output_file = open(f"{output_filename}", "w")
            output_file.write(f.text)
            output_file.close()


signal.signal(signal.SIGINT, signal.default_int_handler)
try:
    if args.check:
        check_data(data_root, date_string)
    else:
        download_data(data_root, date_string)
except KeyboardInterrupt:
    sys.exit()

