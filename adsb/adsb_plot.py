#!/usr/bin/env python3

import os, multiprocessing
import numpy as np
import pandas as pd
import rasterio as rio
from numba import njit
from minio import Minio
from rich.progress import track

res_mult = 200


@njit
def calc_density_map(lats, lons) -> int:
    output = np.zeros(
        shape=((180 * res_mult) + 1, (360 * res_mult) + 1), dtype=np.uint32
    )
    for i in range(len(lats)):
        lat = np.abs((lats[i] - 90)) * res_mult  # convert from -90 -> 90 to 0 -> 181
        # but then other stuff because 0,0 is top left
        lon = (lons[i] + 180) * res_mult  # convert from -180 -> 180 to 0 -> 361

        # print(f"{lats[i]} -> {lat} -> {int(np.round(lat))} in {len(output[0])}")
        # print(f"{lons[i]} -> {lon} -> {int(np.round(lon))} in {len(output)}")
        # break

        output[int(np.round(lat))][int(np.round(lon))] += 1

    return output


def create_density_map(adsb_track_file: str, output_file: str):
    df = pd.read_parquet(adsb_track_file, columns=["latitude_deg", "longitude_deg"])
    a = calc_density_map(df.latitude_deg.to_numpy(), df.longitude_deg.to_numpy())
    dataset = rio.open(
        output_file,
        "w",
        driver="GTiff",
        compress="zstd",
        # zstd_level=22,
        # zstd_level=12,
        zstd_level=6,
        num_threads=multiprocessing.cpu_count(),
        height=len(a),
        width=len(a[0]),
        count=1,
        dtype=a.dtype,
        crs="+proj=latlong",
        transform=rio.Affine(
            1.0 / res_mult, 0, -180, 0, -(1.0 / res_mult), 90, 0, 0, 1
        ),
    )
    dataset.write(a, 1)
    dataset.close()


if __name__ == "__main__":
    s3_host = "192.168.86.25:9768"
    bucket = "adsb"
    client = Minio(s3_host, secure=False)

    if not client.bucket_exists(bucket):
        raise Exception(f"ERROR: Minio bucket '{bucket}' doesn't exist at '{s3_host}'")

    track_objs = list(client.list_objects(bucket, "track_data/"))
    map_objs = list(client.list_objects(bucket, "density_maps/"))
    map_files = []
    for m in map_objs:
        map_files.append(m.object_name)
    map_files = set(map_files)

    for to in track(track_objs, description="Calculating Density Maps..."):
        track_obj = to.object_name
        track_file = os.path.basename(track_obj)
        map_file = f"{track_file.split(".")[0]}.tiff"
        map_obj = f"density_maps/{map_file}"

        if map_obj not in map_files:
            #print(f"{track_obj} -> {map_obj}")
            #print(f"{track_file} -> {map_file}")

            client.fget_object(bucket, track_obj, track_file)
            create_density_map(track_file, map_file)
            client.fput_object(bucket, map_obj, map_file)

            os.remove(track_file)
            os.remove(map_file)
