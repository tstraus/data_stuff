#!/usr/bin/env python3

import argparse
import os
import sys
import shutil
import orjson
import signal
import datetime
import duckdb
import numpy as np
import polars as pl
import adsb_plot as plt

# from multiprocessing import Pool, cpu_count
from rich.progress import Progress, track
from minio import Minio


def chunkify(lst, n):
    return list(lst[i::n] for i in range(n))


def adsb_to_df(adsb_filename: str):
    with open(adsb_filename, "r") as adsb_file:
        time = []
        latitude = []
        longitude = []
        altitude_geom = []
        altitude_baro = []
        altitude_rate = []
        ground_speed = []
        track = []
        nav_heading = []
        iaco_id = []
        aircraft_registration = []
        squawk = []
        aircraft_type = []
        flight = []

        adsb_data = None
        try:
            adsb_data = orjson.loads(adsb_file.read())
        except UnicodeDecodeError:
            print(f"ERROR: '{adsb_filename}' doesn't seem like unicode, skipping")
            return None
        except orjson.JSONDecodeError:
            print(f"ERROR: Unable to parse '{adsb_filename}', skipping")
            return None

        now = datetime.datetime.fromtimestamp(float(adsb_data["now"]), datetime.UTC)
        # print(now.strftime('%Y-%m-%dT%H:%M:%SZ'))

        aircraft = adsb_data["aircraft"]

        for a in aircraft:

            if (
                "type" in a
                and (a["type"] == "adsb_icao" or a["type"] == "adsc")
                and "lat" in a
                and "lon" in a
            ):
                time.append(now)
                latitude.append(float(a["lat"]))
                longitude.append(float(a["lon"]))

                if "alt_baro" in a:
                    v = a["alt_baro"]
                    if isinstance(v, int):
                        altitude_baro.append(float(v))
                    elif isinstance(v, float):
                        altitude_baro.append(v)
                    elif isinstance(v, str) and v == "ground":
                        altitude_baro.append(-1.0)
                else:
                    altitude_baro.append(0.0)

                if "alt_geom" in a:
                    v = a["alt_geom"]
                    if isinstance(v, int):
                        altitude_geom.append(float(v))
                    elif isinstance(v, float):
                        altitude_geom.append(v)
                    elif isinstance(v, str) and v == "ground":
                        altitude_geom.append(-1.0)
                else:
                    altitude_geom.append(0.0)

                if "track" in a:
                    track.append(float(a["track"]))
                else:
                    track.append(0.0)

                if "nav_heading" in a:
                    nav_heading.append(float(a["nav_heading"]))
                else:
                    nav_heading.append(0.0)

                if "baro_rate" in a:
                    altitude_rate.append(float(a["baro_rate"]))
                elif "geom_rate" in a:
                    altitude_rate.append(float(a["geom_rate"]))
                else:
                    altitude_rate.append(0.0)

                if "gs" in a:
                    ground_speed.append(float(a["gs"]))
                else:
                    ground_speed.append(0.0)

                if "hex" in a:
                    iaco_id.append(str(a["hex"]))
                else:
                    iaco_id.append("")

                if "r" in a:
                    aircraft_registration.append(str(a["r"]))
                else:
                    aircraft_registration.append("")

                if "t" in a:
                    aircraft_type.append(str(a["t"]))
                else:
                    aircraft_type.append("")

                if "squawk" in a:
                    squawk.append(str(a["squawk"]))
                else:
                    squawk.append("squawk")

                if "flight" in a:
                    flight.append(str(a["flight"]).strip())
                else:
                    flight.append("")

        df = pl.DataFrame(
            {
                "latitude_deg": np.asarray(latitude, dtype=np.float32),
                "longitude_deg": np.asarray(longitude, dtype=np.float32),
                "altitude_geom_ft": np.asarray(altitude_geom, dtype=np.float32),
                "altitude_baro_ft": np.asarray(altitude_baro, dtype=np.float32),
                "altitude_rate_ft_min": np.asarray(altitude_rate, dtype=np.float32),
                "ground_speed_kts": np.asarray(ground_speed, dtype=np.float32),
                "track_deg": np.asarray(track, dtype=np.float32),
                "nav_heading_deg": np.asarray(nav_heading, dtype=np.float32),
                "icao_id": iaco_id,
                "registration": aircraft_registration,
                "mode_a_squawk": squawk,
                "type": aircraft_type,
                "flight": flight,
                "time": time,
            }
        )

        return df


def process_month_to_parquet(
    data_root: str, year: int, month: int, day: int, clean: bool = False
) -> str:
    filenames = []
    for root, dirs, files in os.walk(f"{data_root}/{year}/{month}/{day}"):
        for file in files:
            filenames.append(f"{data_root}/{year}/{month}/{day}/{file}")

    filenames.sort()
    num_filenames = len(filenames)
    # filenames = chunkify(filenames, int(len(filenames) / (cpu_count() * 100)))

    db_filename = f"{data_root}/adsb_{year}-{month}-{day}.duckdb"
    output_filename = f"{data_root}/adsb_{year}-{month}-{day}.parquet"

    con = duckdb.connect(db_filename)
    con.sql("CREATE TABLE IF NOT EXISTS processed (filename VARCHAR);")
    processed = set(
        con.sql("SELECT * FROM processed").pl().get_column("filename").to_list()
    )

    signal.signal(signal.SIGINT, signal.default_int_handler)
    first = True

    try:
        for f in track(filenames, description="Parsing..."):
            if f not in processed:
                df = adsb_to_df(f)
                if df is not None:
                    if first:
                        con.sql("CREATE TABLE IF NOT EXISTS adsb as SELECT * FROM df;")
                        first = False
                    else:
                        con.sql("INSERT INTO adsb SELECT * FROM df;")

                    file_df = pl.DataFrame({"filename": [f]})
                    con.sql("INSERT INTO processed SELECT * FROM file_df;")

        print("sorting...", end="", flush=True)
        con.sql("DROP TABLE IF EXISTS adsb_sorted;")
        con.sql("CREATE TABLE adsb_sorted AS SELECT * FROM adsb ORDER BY time;")
        #con.sql("DROP TABLE adsb;")
        print("  complete")

        print("compressing...", end="", flush=True)
        con.sql(
            f"COPY adsb_sorted to '{output_filename}' (FORMAT 'parquet', CODEC 'ZSTD', COMPRESSION_LEVEL 22);"
        )
        con.close()

        if clean:
            os.remove(db_filename)

        print("  complete")

    except KeyboardInterrupt:
        # try:
        #    os.remove(db_filename)
        # except OSError:
        #    pass

        sys.exit()

    return output_filename


def upload_to_minio(data_filename: str, map_filename: str):
    s3_host = "192.168.86.25:9768"
    bucket = "adsb"
    data_object_name = os.path.basename(data_filename)
    map_object_name = os.path.basename(map_filename)

    client = Minio(s3_host, secure=False)

    if not client.bucket_exists(bucket):
        raise Exception(f"ERROR: Minio bucket '{bucket}' doesn't exist at '{s3_host}'")

    print(f"Uploading '{data_filename}' to '{s3_host}/{bucket}/track_data' as '{data_object_name}'")
    client.fput_object(bucket, f"track_data/{data_object_name}", data_filename)
    print(f"Uploading '{map_filename}' to '{s3_host}/{bucket}/density_maps' as '{map_object_name}'")
    client.fput_object(bucket, f"density_maps/{map_object_name}", map_filename)
    print("  complete")


if __name__ == "__main__":
    year = ""
    month = ""
    day = ""

    parser = argparse.ArgumentParser()
    parser.add_argument("year", type=int)
    parser.add_argument("month", type=int)
    parser.add_argument("day", type=int)
    parser.add_argument("-c", "--clean", action="store_true")
    parser.add_argument("-d", "--data", default="../data/adsb/")
    args = parser.parse_args()

    data_root = args.data
    year = str(args.year)
    if args.month < 10:
        month = f"0{args.month}"
    else:
        month = str(args.month)
    if args.day < 10:
        day = f"0{args.day}"
    else:
        day = str(args.day)

    data_filename = process_month_to_parquet(data_root, year, month, day, args.clean)
    map_filename = data_filename.replace(".parquet", ".tiff")

    plt.create_density_map(data_filename, map_filename)

    upload_to_minio(data_filename, map_filename)

    if args.clean:
        print("cleanup")
        os.remove(data_filename)
        os.remove(map_filename)
        shutil.rmtree(f"{data_root}/{year}/{month}/{day}")
