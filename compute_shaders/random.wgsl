@group(0) @binding(0)
var<storage,read> time: array<f32>;

@group(0) @binding(1)
var<storage,read_write> output: array<f32>;

// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
fn hash(_x: u32) -> u32 {
    var x = _x;
    x += (x << 10u);
    x ^= (x >> 6u);
    x += (x << 3u);
    x ^= (x >> 11u);
    x += (x << 15u);

    return x;
}

// Compound versions of the hashing algorithm I whipped together.
fn hash2(v: vec2<u32>) -> u32 { return hash(v.x^hash(v.y)); }
fn hash3(v: vec3<u32>) -> u32 { return hash(v.x^hash(v.y)^hash(v.z)); }
fn hash4(v: vec4<u32>) -> u32 { return hash(v.x^hash(v.y)^hash(v.z)^hash(v.w)); }

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
fn floatConstruct(_m: u32) -> f32 {
    var m = _m;
    let ieeeMantissa: u32 = 0x007FFFFFu; // binary32 mantissa bitmask
    let ieeeOne: u32 = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    let f: f32 = bitcast<f32>(m);       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}

// Pseudo-random value in half-open range [0:1].
fn random(x: f32) -> f32 { return floatConstruct(hash(bitcast<u32>(x))); }
fn random2(v: vec2<f32>) -> f32 { return floatConstruct(hash2(bitcast<u32>(v))); }
fn random3(v: vec3<f32>) -> f32 { return floatConstruct(hash3(bitcast<u32>(v))); }
fn random4(v: vec4<f32>) -> f32 { return floatConstruct(hash4(bitcast<u32>(v))); }

@compute @workgroup_size(64)
fn main(@builtin(global_invocation_id) id: vec3<u32>) {
    if (id.x >= arrayLength(&output)) {
        return; // do nothing when over-dispatched
    }
    let i: u32 = id.x;
    // use the vec2 version so we can use both time and
    // index to get different values across whole output array
    output[i] = random2(vec2<f32>(time[0], f32(i)));
}
