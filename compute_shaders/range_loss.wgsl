@group(0) @binding(0)
var<storage,read> powers: array<f32>;

@group(0) @binding(1)
var<storage,read> ranges: array<f32>;

@group(0) @binding(2)
var<storage,read_write> output: array<f32>;

fn range_loss(power: f32, range: f32) -> f32 {
    return power / pow(4.0*3.14159*range, 2.0);
}

@compute @workgroup_size(1)
fn main(@builtin(global_invocation_id) index: vec3<u32>) {
    let i: u32 = index.x;
    output[i] = range_loss(powers[i], ranges[i]);
}
