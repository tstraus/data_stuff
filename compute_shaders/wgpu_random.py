#!/usr/bin/env python3

import wgpu
import wgpu.backends.auto  # Select backend
from wgpu.utils.compute import compute_with_buffers  # Convenience function
import numpy as np
import time
import math

print("WGSL random numbers")

# shader and data
shader_source = open("./random.wgsl", "r").read()

# numpy array input
count = 1000000
t = np.array([time.time()])

# execute
start = time.perf_counter_ns()
result = compute_with_buffers(
    { # input buffers
        0: t,
    },
    { # output buffers
        1: count * np.dtype(np.float32).itemsize # calculate output buffer size
    },
    shader_source, # shader to execute
    n=(math.ceil(count/64.0), 64, 1)
    # chunk buffer otherwise max inputs are 2^16
    # using 64 to match shader workgroup size
    # ceil to over-dispatch to ensure array is filled
)
elapsed_ms = (time.perf_counter_ns() - start) / 1000000.0

# get output as numpy array
output = np.frombuffer(result[1], dtype=np.float32)

print(f"num: {count}")
print(f"time: {elapsed_ms}ms")
print(f"mean: {np.mean(output)}")
print(f"median: {np.median(output)}")
