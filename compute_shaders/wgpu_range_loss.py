#!/usr/bin/env python3

import wgpu
import wgpu.backends.auto  # Select backend
from wgpu.utils.compute import compute_with_buffers  # Convenience function
import numpy as np
from bokeh.io import curdoc
from bokeh.plotting import figure, show

# shader and data
shader_source = open("./range_loss.wgsl", "r").read()

# numpy array input
count = 10000
powers = np.full(count, 98.0, dtype=np.float32)
ranges = np.linspace(0.1, 200.1, num=count, dtype=np.float32)

# execute
out = compute_with_buffers(
    { # input buffers
        0: powers,
        1: ranges
    },
    { # output buffers
        2: powers.nbytes
    },
    shader_source # shader to execute
)

# get output as numpy array
result = np.frombuffer(out[2], dtype=np.float32)
#print(result)

curdoc().theme = "dark_minimal"
p = figure(
    title="Range Loss",
    #sizing_mode="scale_width",
    y_axis_type="log"
)
p.line(ranges, result)
show(p)

