#!/usr/bin/env python3

import argparse
import csv
import itertools
import pandas
from pyarrow import parquet
from bokeh.io import curdoc
from bokeh.plotting import figure, show
from bokeh.palettes import Bokeh8 as palette

pandas.options.mode.chained_assignment = None


# dot.notation access to dictionary attributes
class dotdict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


fields = dotdict(
    {
        "cases": "Cases",
        "cases_velocity": "Cases Velocity",
        "cases_acceleration": "Cases Acceleration",
        "cases_per_capita": "Cases Per Capita",
        "cases_velocity_per_capita": "Cases Velocity Per Capita",
        "cases_acceleration_per_capita": "Cases Acceleration Per Capita",
        "deaths": "Deaths",
        "deaths_velocity": "Deaths Velocity",
        "deaths_acceleration": "Deaths Acceleration",
        "deaths_per_capita": "Deaths Per Capita",
        "deaths_velocity_per_capita": "Deaths Velocity Per Capita",
        "deaths_acceleration_per_capita": "Deaths Acceleration Per Capita",
    }
)

# build up a list of valid fields for display
all_fields_str = f"valid options: "
for field in fields.items():
    all_fields_str = all_fields_str + '"' + field[1] + '" '

# get some arguments from the user
parser = argparse.ArgumentParser(
    description="Make plots of covid data",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-i",
    "--interpolate",
    dest="interpolate",
    action="store_true",
    help="interpolate data",
)
parser.add_argument(
    "-r", "--raw", dest="raw", action="store_true", help="don't smooth the data"
)
parser.add_argument(
    "-c",
    "--countries",
    dest="countries",
    help='list of countries to plot. Ex: "US,China,Venezuela,Guatemala,Italy,Brazil,India"',
)
parser.add_argument(
    "-s",
    "--states",
    dest="states",
    default="Maryland,Puerto Rico,Virginia,Texas,New York,Pennsylvania",
    help="list of states to plot",
)
parser.add_argument(
    "-f",
    "--field",
    dest="field",
    default=fields.cases_velocity_per_capita,
    help=all_fields_str,
)
args = parser.parse_args()


def calculate_additional_fields(covid_data, populations, areas):
    if args.interpolate:
        covid_data.Cases = covid_data.Cases.interpolate(method="cubic")
        covid_data.Deaths = covid_data.Deaths.interpolate(method="cubic")
    elif not args.raw:
        covid_data.Cases = covid_data.Cases.rolling(7).sum()
        covid_data.Deaths = covid_data.Deaths.rolling(7).sum()

    # calculate the rate of change
    covid_data[fields.cases_velocity] = covid_data.Cases.diff()
    covid_data[fields.deaths_velocity] = covid_data.Deaths.diff()
    covid_data[fields.cases_acceleration] = covid_data[fields.cases_velocity].diff()
    covid_data[fields.deaths_acceleration] = covid_data[fields.deaths_velocity].diff()

    # get some more countries
    covid_calculations = {}
    for area in areas:
        covid_calculations[area] = covid_data.loc[covid_data["Area"] == area]
        covid_calculations[area][fields.cases_per_capita] = (
            covid_calculations[area][fields.cases] / populations[area]
        )
        covid_calculations[area][fields.deaths_per_capita] = (
            covid_calculations[area][fields.deaths] / populations[area]
        )
        covid_calculations[area][fields.cases_velocity_per_capita] = (
            covid_calculations[area][fields.cases_velocity] / populations[area]
        )
        covid_calculations[area][fields.deaths_velocity_per_capita] = (
            covid_calculations[area][fields.deaths_velocity] / populations[area]
        )
        covid_calculations[area][fields.cases_acceleration_per_capita] = (
            covid_calculations[area][fields.cases_acceleration] / populations[area]
        )
        covid_calculations[area][fields.deaths_acceleration_per_capita] = (
            covid_calculations[area][fields.deaths_acceleration] / populations[area]
        )

        # trim dates too early to correctly calculate these fields
        covid_calculations[area] = covid_calculations[area].loc[
            pandas.to_datetime("2020-03-21") : pandas.to_datetime("today")
        ]

    return covid_calculations


def make_covid_plot(covid_data, areas):
    # plot it
    curdoc().theme = "dark_minimal"
    colors = itertools.cycle(palette)
    p = figure(
        title=f"Covid 19 {args.field}",
        x_axis_type="datetime",
        sizing_mode="scale_width",
    )
    for area, color in zip(areas, colors):
        c = covid_data[area]

        p.line(c.index, c[args.field], legend_label=area, color=color)

    p.legend.location = "top_left"
    show(p)


def plot_countries():
    # parse the list of countries
    reader = csv.reader(args.countries.splitlines())
    countries = next(reader)

    # load reference data for populations
    populations = {}
    reference_data = parquet.read_table(
        "data/parquet/covid/reference.parquet"
    ).to_pandas()
    for country in countries:
        populations[country] = reference_data.loc[
            reference_data["Combined_Key"] == country
        ].iloc[0]["Population"]

    # load source data, and make the column names match
    covid = (
        parquet.read_table("data/parquet/covid/countries-aggregated.parquet")
        .to_pandas()
        .rename(columns={"Confirmed": fields.cases, "Country": "Area"})
    )
    # index by date
    covid["Date"] = pandas.to_datetime(covid["Date"])
    covid.set_index("Date", inplace=True)

    make_covid_plot(
        calculate_additional_fields(covid, populations, countries), countries
    )


def plot_states():
    # parse the list of states
    reader = csv.reader(args.states.splitlines())
    states = next(reader)

    # load reference data for populations
    populations = {}
    reference_data = parquet.read_table(
        "data/parquet/covid/reference.parquet"
    ).to_pandas()

    for state in states:
        populations[state] = reference_data.loc[
            reference_data["Province_State"] == state
        ].iloc[0]["Population"]

    # load source data, and make the column names match
    covid = (
        parquet.read_table("data/parquet/covid_nytimes/us-states.parquet")
        .to_pandas()
        .sort_values(by=["state", "date"])
        .rename(
            columns={
                "cases": fields.cases,
                "deaths": fields.deaths,
                "state": "Area",
                "date": "Date",
            }
        )
    )
    # index by date
    covid["Date"] = pandas.to_datetime(covid["Date"])
    covid.set_index("Date", inplace=True)

    make_covid_plot(calculate_additional_fields(covid, populations, states), states)


if args.countries is None:
    plot_states()
else:
    plot_countries()
