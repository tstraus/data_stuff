#!/usr/bin/env python

import os
from multiprocessing import Pool
from pyarrow import csv
from pyarrow import parquet
from pyarrow import feather
from rich.progress import Progress

def convertFileFeather(names):
    in_name = names[0]
    out_name = names[1]

    #print(in_name, " -> ", out_name)

    feather.write_feather(csv.read_csv(in_name), out_name, compression="zstd", compression_level=22)

def convertFileParquet(names):
    in_name = names[0]
    out_name = names[1]

    #print(in_name, " -> ", out_name)

    parquet.write_table(csv.read_csv(in_name), out_name, compression="zstd", compression_level=22)

def csv_to_binary(dir):
    jobs_feather = []
    jobs_parquet = []

    for root, dirs, files in os.walk(dir + "/csv/"):
        for file in files:
            if file.endswith((".csv")):
                out_dir_feather = root.replace("csv", "feather")
                out_dir_parquet = root.replace("csv", "parquet")

                if not os.path.exists(out_dir_feather):
                    os.makedirs(out_dir_feather)

                if not os.path.exists(out_dir_parquet):
                    os.makedirs(out_dir_parquet)

                old_name = root + "/" + file
                new_name_feather = out_dir_feather + "/" + os.path.splitext(file)[0] + ".feather"
                new_name_parquet = out_dir_parquet + "/" + os.path.splitext(file)[0] + ".parquet"

                jobs_feather.append([old_name, new_name_feather])
                jobs_parquet.append([old_name, new_name_parquet])

    #with Pool() as pool, Progress() as progress:
    #    task = progress.add_task("csv -> feather: ", total=len(jobs_feather))
    #    for _ in pool.imap(convertFileFeather, jobs_feather):
    #        progress.update(task, advance=1)
    with Pool() as pool, Progress() as progress:
        task = progress.add_task("csv -> parquet: ", total=len(jobs_parquet))
        for _ in pool.imap(convertFileParquet, jobs_parquet):
            progress.update(task, advance=1)

if __name__ == "__main__":
    csv_to_binary("data")
