import numpy as np
import pandas as pd
import matplotlib.pyplot as mp
from filterpy.kalman import KalmanFilter

data_points = 500
burnin = 150


def filter(ys):
    global burnin
    kf = KalmanFilter(dim_x=1, dim_z=1)
    kf.F = np.array([[1.0]])
    kf.H = np.array([[1.0]])
    # "Fit" the filter ... library does not seem to provide this function
    q_var = np.var(ys[:burnin])
    kf.Q = np.array([[q_var]])
    kf.R = np.array([[q_var]])
    x_rest = list()
    kf.x = np.array([0])
    kf.P *= 10.0
    ys_test = ys[burnin:]
    for y in ys_test:
        kf.predict()
        x_rest.append(kf.x[0])
        kf.update([y])
        # Predict does not return a value. Grumble
    return np.append(ys[:burnin], x_rest)


covid = pd.read_parquet("data/parquet/covid/countries-aggregated.parquet")
covid["Date"] = pd.to_datetime(covid["Date"])
covid.set_index("Date", inplace=True)
covid = covid.loc[covid["Country"] == "US"]
covid["Velocity"] = covid.Confirmed.diff()
# covid["Velocity"] = covid.Confirmed.rolling(7).sum().diff()
covid = covid[-data_points:]
print(covid)

input = covid.Velocity.to_numpy()
output = filter(input)
print(len(output))

#mp.scatter(np.arange(0, len(input) - burnin), input[burnin:])
#mp.plot(np.append(np.zeros(burnin), output), color="orange")
mp.scatter(np.arange(0, len(input)), input)
mp.plot(output, color="orange")
mp.show()
