#!/usr/bin/env python3

from nicegui import ui
from nicegui.events import ValueChangeEventArguments
import torch
from torchvision.io import read_image, write_jpeg

dtype = torch.float
if torch.cuda.is_available():
    device = torch.device("cuda")
elif torch.backends.mps.is_available() and torch.backends.mps.is_built():
    device = torch.device("mps")
else:
    device = torch.device("cpu")


input_name = "data/images/input.jpg"
output_name = "data/images/output.jpg"


def switch_channel(event: ValueChangeEventArguments):
    #print(f"new channel: {event.value}")
    match event.value:
        case "All":
            output_image_display.set_source(input_name)
        case "R" | "G" | "B":
            offset = 0 # R
            if event.value == "G":
                offset = 1
            elif event.value == "B":
                offset = 2

            image = read_image(input_name)
            image = image[offset:offset+1]
            write_jpeg(image, output_name, 100)
            output_image_display.set_source(output_name)
            output_image_display.force_reload()

        case _:
            return


def on_button():
    i = read_image(input_name).to(device=device, dtype=dtype)

    noise_factor = 200 # plus or minus
    noise = torch.rand(i.shape, device=device) * (noise_factor * 2) - noise_factor
    i = i + noise
    i[i > 255] = 255
    i[i < 0] = 0

    write_jpeg(i.to(dtype=torch.uint8).to(device="cpu"), output_name, 100)

    output_image_display.set_source(output_name)
    output_image_display.force_reload()


with ui.row():
    ui.radio(["All", "R", "G", "B"], value="All", on_change=switch_channel).props("inline")
    ui.button("Math!", on_click=on_button)


with ui.row().classes('w-full flex items-center'):
    input_image_display = ui.image(input_name).style("width: 48%")
    output_image_display = ui.image(input_name).style("width: 48%")


ui.run(dark=True, native=True)
