#!/usr/bin/env python3

import os
from rich.progress import track
from minio import Minio

s3_host = "192.168.86.25:9768"
client = Minio(s3_host, "tstraus0", "bxkN32TBuuWHQPTqaNci", secure=False)

for bucket in client.list_buckets():
	os.makedirs(bucket.name, exist_ok=True)

	bucket_objects = list(client.list_objects(bucket.name, recursive=True))

	for obj in track(bucket_objects, description=bucket.name):
		objdir = f"{bucket.name}/{os.path.dirname(obj.object_name)}"
		os.makedirs(objdir, exist_ok=True)

		output_filename = f"{bucket.name}/{obj.object_name}"
		if not os.path.exists(output_filename):
			client.fget_object(bucket.name, obj.object_name, output_filename)
