#!/usr/bin/env python3

import matplotlib.pylab as plt
import numpy as np
import scipy.fft as sf
import cv2
import time


def generate_noise(height, width):
    start_time = time.perf_counter_ns()

    input = np.random.uniform(size=[height, width])
    fd = sf.fft2(input)
    fd = sf.fftshift(fd)

    # create blurred circle mask (2D low pass filter)
    radius = 16
    mask = np.zeros_like(input)
    cy = mask.shape[0] // 2  # get center
    cx = mask.shape[1] // 2
    cv2.circle(mask, (cx, cy), radius, 1.0, -1)[0]
    mask = cv2.GaussianBlur(mask, (19, 19), 0)

    fd = np.multiply(fd, mask)
    fd = sf.ifftshift(fd)
    noise = sf.ifft2(fd).real.astype(np.float32)

    # normalize
    min = np.min(noise)
    max = np.max(noise)
    noise = (noise - min) / (max - min)

    elapsed = (time.perf_counter_ns() - start_time) / 1000000  # ns -> ms
    print(f"time: {elapsed}ms")

    return noise


def run_ui(height, width):
    import dearpygui.dearpygui as dpg

    noise = np.zeros([height, width])

    dpg.create_context()

    with dpg.window(label="Noise Viewer") as main_window:
        with dpg.plot(no_mouse_pos=True, height=-1, width=-1):
            dpg.bind_colormap(dpg.last_item(), dpg.mvPlotColormap_Plasma)
            dpg.add_plot_axis(
                dpg.mvXAxis,
                lock_min=True,
                lock_max=True,
                no_gridlines=True,
                no_tick_marks=True,
                no_tick_labels=True,
            )
            dpg.add_plot_axis(
                dpg.mvYAxis,
                no_gridlines=True,
                no_tick_marks=True,
                lock_min=True,
                lock_max=True,
                no_tick_labels=True,
                tag="y_axis",
            )
            dpg.add_heat_series(
                noise,
                width,
                height,
                scale_min=0,
                scale_max=1,
                tag="heatmap",
                parent="y_axis",
                format="",
            )

    dpg.create_viewport(title="Noise Viewer", width=1000, height=1000)
    dpg.set_viewport_vsync(True)
    dpg.setup_dearpygui()
    dpg.set_primary_window(main_window, True)
    dpg.show_viewport()

    while dpg.is_dearpygui_running():
        noise = generate_noise(height, width)
        dpg.configure_item("heatmap", x=noise)

        dpg.render_dearpygui_frame()

    dpg.destroy_context()


def show_noise(height, width):
    import seaborn as sb

    noise = generate_noise(height, width)

    # print(sf.fftfreq(fd.size))
    # print(input[100][100])
    # print(fd[100][100])
    # print(noise[100][100].real)
    # print(noise.dtype)

    ax = sb.heatmap(noise)
    plt.show()


height = 512
width = 512

# run_ui(height, width)
show_noise(height, width)
