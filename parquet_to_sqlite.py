#!/usr/bin/env python3

import os
import sqlite3
import pandas
from multiprocessing import Pool
from pyarrow import parquet

def convert_file(names):
    in_name = names[0]
    out_name = names[1]

    print(in_name, "->", out_name)

    db = sqlite3.connect(out_name)

    try:
        parquet.read_table(in_name).to_pandas().to_sql("data", db)
    finally:
        print("uh oh")

    db.commit()
    db.close()

def parquet_to_sqlite(dir):
    jobs = []

    for root, dirs, files in os.walk(dir + "/parquet/"):
        out_dir = root.replace("parquet", "sqlite")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        for file in files:
            if file.endswith((".parquet")):
                old_name = root + "/" + file
                new_name = out_dir + "/" + os.path.splitext(file)[0] + ".db"

                jobs.append([old_name, new_name])

    with Pool() as pool:
        pool.map(convert_file, jobs)

if __name__ == "__main__":
    parquet_to_sqlite("data")
