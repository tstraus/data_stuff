#!/usr/bin/env julia

using CUDA
using BenchmarkTools

loops = 1000
reps = 100000000

function single_pi(i)
    x_s = CUDA.rand(Float32, reps)
    y_s = CUDA.rand(Float32, reps)

    x_s = x_s .* x_s
    y_s = y_s .* y_s

    x_s = x_s .+ y_s

    return count(x->(x <= 1.0), x_s)
end

function loop_pi(loops)
    total = loops * reps

    return mapreduce(single_pi, +, 1:loops, init=0) / total * 4.0
end

@btime loop_pi(1)

@time our_pi = loop_pi(loops)
println("pi: $our_pi, iterations: $(loops * reps)")
