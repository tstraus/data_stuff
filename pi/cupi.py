#!/usr/bin/env python

import time
import cupy as cp

loops = 10000 # 10,000
reps = 10000000 # 10,000,000
total = loops * reps

@cp.fuse()
def in_unit_circle(x):
    return x <= 1.0

start = time.perf_counter_ns()
in_counts = cp.zeros(loops, dtype=cp.int32)

for i in range(0, loops):
    x = cp.random.uniform(size=reps, dtype=cp.float32)
    y = cp.random.uniform(size=reps, dtype=cp.float32)

    x = x * x
    y = y * y
    x = x + y

    x = in_unit_circle(x)
    in_counts[i] = cp.count_nonzero(x)

    #y = cp.ones(reps, dtype=cp.float32)
    #in_counts[i] = cp.count_nonzero(x < y)

pi = cp.sum(in_counts) / total * 4.0
elapsed_time = (time.perf_counter_ns() - start) / 1000000 # ns -> ms

device_props = cp.cuda.runtime.getDeviceProperties(0)
threads = device_props["multiProcessorCount"] * device_props["maxThreadsDim"][2]
print(f"threads: {threads}")
print(f"samples: {total}")
print(f"pi: {pi}")
print(f"time: {elapsed_time}ms")
