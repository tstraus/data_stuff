#!/usr/bin/env python

import time
import jax
import numpy as np
import jax.numpy as jnp

loops = 10000 # 10,000
reps = 10000000 # 10,000,000
total = loops * reps

start = time.perf_counter_ns()
in_counts = jnp.zeros(loops, dtype=jnp.int32)

key = jax.random.PRNGKey(time.time_ns()) # Random seed is explicit in JAX

for i in range(0, loops):
    key, xkey = jax.random.split(key)
    key, ykey = jax.random.split(key)

    x = jax.random.uniform(xkey, shape=(reps,), dtype=jnp.float32)
    y = jax.random.uniform(ykey, shape=(reps,), dtype=jnp.float32)

    x = x * x
    y = y * y
    x = x + y

    y = jnp.ones(reps, dtype=jnp.float32)
    in_counts = in_counts.at[i].set(jnp.count_nonzero(x < y))

pi = np.array(in_counts).astype(np.int64).sum() / total * 4.0
elapsed_time = (time.perf_counter_ns() - start) / 1000000 # ns -> ms

print(f"samples: {total}")
print(f"pi: {pi}")
print(f"time: {elapsed_time}ms")
