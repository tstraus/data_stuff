#!/usr/bin/env python3

from numba import njit, prange
import numpy as np
import time

loops = 10000 # 10,000
reps = 10000000 # 10,000,000
total = loops * reps

rngs = [np.random.default_rng() for _ in range(loops)]

@njit(parallel=True)
def monte_carlo_pi(rngs, loops, reps):
    in_counts = np.zeros(loops, dtype=np.uint32)

    for i in prange(loops):
        x = rngs[i].random(reps, dtype=np.float32)
        y = rngs[i].random(reps, dtype=np.float32)

        x = x * x
        y = y * y
        x = x + y

        y = np.ones(reps, dtype=np.float32)
        in_counts[i] = np.count_nonzero(x < y)

    return np.sum(in_counts) / float(total) * 4.0

# make sure it's already optimized
monte_carlo_pi(rngs, 1, 1)

start = time.perf_counter_ns()
print(f'pi: {monte_carlo_pi(rngs, loops, reps)}')
elapsed = (time.perf_counter_ns() - start) / 1000000
print(f"time: {elapsed}ms")
