#!/usr/bin/env python

import time
import mlx.core as mx

loops = 1000 # 10,000
reps = 10000000 # 10,000,000
total = loops * reps

start = time.perf_counter_ns()
in_counts = mx.zeros(loops)

for i in range(0, loops):
    x = mx.random.uniform(shape=[reps])
    y = mx.random.uniform(shape=[reps])

    x = x * x
    y = y * y
    x = x + y

    y = mx.ones(reps, dtype=mx.float32)
    in_counts[i] = mx.sum(x < y)

pi = mx.sum(in_counts).item() / total * 4.0
elapsed_time = (time.perf_counter_ns() - start) / 1000000 # ns -> ms

print(f"samples: {total}")
print(f"pi: {pi}")
print(f"time: {elapsed_time}ms")
