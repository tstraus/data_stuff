#!/usr/bin/env python3

import time
import torch

loops = 10000 # 10,000
size = 10000000 # 10,000,000
total = loops * size

dtype = torch.float
if torch.cuda.is_available():
    device = torch.device("cuda")
elif torch.backends.mps.is_available() and torch.backends.mps.is_built():
    device = torch.device("mps")
else:
    device = torch.device("cpu")

start_time = time.perf_counter_ns()
in_counts = torch.zeros(loops, dtype=torch.int32)

for i in range(0, loops):
    x = torch.rand(size, dtype=dtype, device=device)
    y = torch.rand(size, dtype=dtype, device=device)

    x = x * x
    y = y * y
    x = x + y
    y = torch.ones(size, dtype=dtype, device=device)

    in_counts[i] = (x < y).count_nonzero()

pi = torch.sum(in_counts) / (total / 4)

elapsed = (time.perf_counter_ns() - start_time) / 1000000 # ns -> ms

print(f"samples: {total}")
print(f"pi: {pi.item()}")
print(f"time: {elapsed}ms")

