#!/usr/bin/env python3

import time
import cupy as cp
#import numpy as cp

@cp.fuse()
def range_loss(power, range):
    return power / pow(4.0 * cp.pi * range, 2.0)

count = 100000
powers = cp.full(count, 98.0, dtype=cp.float32)
ranges = cp.linspace(0.1, 200.1, num=count, dtype=cp.float32)

result = range_loss(powers, ranges)
print(result)

