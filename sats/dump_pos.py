#!/usr/bin/env python3

import pandas as pd
import sats as s

sats = s.load_sats_at_now("../data/sats/tle/active.tle")
sats.to_csv("~/Desktop/sats.csv")
print(sats)
