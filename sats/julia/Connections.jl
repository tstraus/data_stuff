module Connections

export get_connections, create_graph, get_range, path_length, get_closest_sat

using DataFrames
using Graphs
using SimpleWeightedGraphs
using Geodesy

function get_n_closest(sat, sats::DataFrame, n::Int)::DataFrame
    distance_x = sat.x .- sats.x
    distance_y = sat.y .- sats.y
    distance_z = sat.z .- sats.z

    closest = DataFrame()
    closest.range = sqrt.(
        distance_x .* distance_x .+
        distance_y .* distance_y .+
        distance_z .* distance_z
    )
    closest.index = collect(1:length(closest.range))
    sort!(closest, :range)
    closest = closest[2:1+n, :]

    return closest
end

function get_closest_sat(station::ECEF, sats::DataFrame,)::Tuple{Float64, Int64}
    d_x = station.x .- sats.x
    d_y = station.y .- sats.y
    d_z = station.z .- sats.z

    range = sqrt.(d_x .* d_x .+ d_y .* d_y .+ d_z .* d_z)
    return findmin(range)
end

function print_closest(sats::DataFrame, n::Int)
    for sat in eachrow(sats)
        closest = get_n_closest(sat, sats, n)
        println("$(sat.name) ->")

        for close in eachrow(closest)
            println("   $(sats.name[close.index]): $(close.range) km")
        end
    end
end

function get_connections(sats::DataFrame, n::Int)::DataFrame
    connections = DataFrame()

    for (i, sat) in enumerate(eachrow(sats))
        closest = get_n_closest(sat, sats, n)

        for c in eachrow(closest)
            push!(connections, (from_sat=i, to_sat=c.index, range=c.range))
        end
    end

    return connections
end

function create_graph(connections::DataFrame)::SimpleWeightedGraph
    return SimpleWeightedGraph(
        connections.from_sat,
        connections.to_sat,
        connections.range
    )
end

function get_range(sats::DataFrame, a_index::Int, b_index::Int)::Float64
    distance_x = sats.x[a_index] - sats.x[b_index]
    distance_y = sats.y[a_index] - sats.y[b_index]
    distance_z = sats.z[a_index] - sats.z[b_index]

    return sqrt(
        distance_x * distance_x +
        distance_y * distance_y +
        distance_z * distance_z
    )
end

function get_range(a::ECEF, b::ECEF)::Float64
    distance_x = a.x - b.x
    distance_y = a.y - b.y
    distance_z = a.z - b.z

    return sqrt(
        distance_x * distance_x +
        distance_y * distance_y +
        distance_z * distance_z
    )
end

function path_length(sats::DataFrame, path::Vector{SimpleWeightedEdge{Int64,Float64}})::Float64
    length = 0.0

    for p in path
        length += get_range(sats, src(p), dst(p))
    end

    if length == 0.0
        length = Inf
    end

    return length
end

end


