module Latency

export get_path_latency_ms, get_avg_latency_ms

using Distributions

function get_latency_us(mean_us, min_us)
	dist = Gamma(2, 2)
	return rand(dist) * (mean_us / 8) + min_us
end

function get_path_latency_ms(distance_km, num_sats)
	min_latency = 250
	mean_latency = 500
	drop_latency = mean_latency * 3.7

	latencies = [get_latency_us(mean_latency, min_latency) for _ in 1:num_sats]

	if maximum(latencies) > drop_latency
		return -1
	end

	sat_latency = sum(latencies) * 0.001 # us -> ms

	return sat_latency + (distance_km / 299.792458) # speed of light in km / ms
end

function get_avg_latency_ms(distance_km, num_sats, samples)
	if num_sats <= 0
		return [Inf, Inf]
	end

	latencies = []
	drop_count = 0
	for sample in 1:samples
		result = get_path_latency_ms(distance_km, num_sats)
		if result == -1
			drop_count += 1
		else
			push!(latencies, result)
		end
	end

	return [mean(latencies), drop_count / samples * 100.0]
end

end
