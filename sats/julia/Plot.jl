module Plot

export plot_sats

using GLMakie
using DataFrames
using Graphs
using Geodesy
using SimpleWeightedGraphs

function get_connections_points(sats::DataFrame, connections::DataFrame)::Vector{Pair{Point3f,Point3f}}
    connection_points = Vector{Pair{Point3f,Point3f}}()

    for conn in eachrow(connections)
        push!(connection_points,
            Pair(Point3f(
                    sats.x[conn.from_sat],
                    sats.y[conn.from_sat],
                    sats.z[conn.from_sat]
                ),
                Point3f(
                    sats.x[conn.to_sat],
                    sats.y[conn.to_sat],
                    sats.z[conn.to_sat]
                ))
        )
    end

    return connection_points
end

function get_path_points(sats::DataFrame, path::Vector{SimpleWeightedEdge{Int64,Float64}})::Vector{Pair{Point3f,Point3f}}
    path_points = Vector{Pair{Point3f,Point3f}}()

    for e in path
        push!(path_points,
            Pair(Point3f(
                    sats.x[src(e)],
                    sats.y[src(e)],
                    sats.z[src(e)]
                ),
                Point3f(
                    sats.x[dst(e)],
                    sats.y[dst(e)],
                    sats.z[dst(e)]
                )
            )
        )
    end

    return path_points
end

function get_path_points(sats::DataFrame, path::Vector{SimpleWeightedEdge{Int64,Float64}},
    transmitter::ECEF, receiver::ECEF)::Vector{Pair{Point3f,Point3f}}
    path_points = Vector{Pair{Point3f,Point3f}}()

    push!(path_points,
        Pair(Point3f(transmitter.x, transmitter.y, transmitter.z),
            Point3f(sats.x[src(path[1])], sats.y[src(path[1])], sats.z[src(path[1])])
        )
    )

    for e in path
        push!(path_points,
            Pair(Point3f(
                    sats.x[src(e)],
                    sats.y[src(e)],
                    sats.z[src(e)]
                ),
                Point3f(
                    sats.x[dst(e)],
                    sats.y[dst(e)],
                    sats.z[dst(e)]
                )
            )
        )
    end

    push!(path_points,
        Pair(
            Point3f(
                sats.x[dst(path[length(path)])],
                sats.y[dst(path[length(path)])],
                sats.z[dst(path[length(path)])]
            ),
            Point3f(receiver.x, receiver.y, receiver.z)
        )
    )

    return path_points
end

function plot_sats(sats::DataFrame, connections::DataFrame, path::Vector{SimpleWeightedEdge{Int64,Float64}})
    # markersize seems to be a little weird across platforms...
    ms = 15

    fig, ax, _ = scatter(sats[:, "x"], sats[:, "y"], sats[:, "z"],
        markersize=ms, color=1:length(sats[:, "x"]), colormap=:viridis
    ) # the sat locations
    _ = mesh!(ax, Sphere(Point3f(0, 0, 0), 6378.14), shading=false, color=:gray) # earth sized sphere

    _ = linesegments!(ax, get_connections_points(sats, connections), linewidth=0.5) # the connections

    _ = linesegments!(ax, get_path_points(sats, path), linewidth=3.5, color=:red) # the path taken

    wait(display(fig))
end

function plot_sats(sats::DataFrame, connections::DataFrame,
    path::Vector{SimpleWeightedEdge{Int64,Float64}}, transmitter::ECEF, receiver::ECEF)
    # markersize seems to be a little weird across platforms...
    ms = 15

    fig, ax, _ = scatter(sats[:, "x"], sats[:, "y"], sats[:, "z"],
        markersize=ms, color=1:length(sats[:, "x"]), colormap=:viridis
    ) # the sat locations
    _ = mesh!(ax, Sphere(Point3f(0, 0, 0), 6378.14), shading=false, color=:gray) # earth sized sphere

    _ = linesegments!(ax, get_connections_points(sats, connections), linewidth=0.5) # the connections

    _ = linesegments!(ax, get_path_points(sats, path, transmitter, receiver), linewidth=3.5, color=:red) # the path taken

    _ = scatter!(ax, [transmitter.x, receiver.x], [transmitter.y, receiver.y], [transmitter.z, receiver.z], markersize=2 * ms, color=:coral)

    wait(display(fig))
end

end

