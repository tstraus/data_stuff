module Stations

export get_transmitter, get_receiver

using Geodesy

function get_transmitter()
	# eiffel tower
	lat = 48.858085 # deg
	lon = 2.294504 # deg
	alt = 35 # m

	return ECEF(LLA(lat, lon, alt), wgs84) * 0.001 # m -> km
end

function get_receiver()
	# arecibo observatory
	lat = 18.344286 # deg
	lon = -66.752700 # deg
	alt = 498 # m

	return ECEF(LLA(lat, lon, alt), wgs84) * 0.001 # m -> km
end

end