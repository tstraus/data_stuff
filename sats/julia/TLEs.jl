module TLEs

export update_tles, load_sats_at, load_sats_at_now

using Dates
using SatelliteToolbox
using DataFrames

function update_tles()
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=active&FORMAT=tle", "../data/sats/tle/active.tle")
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=globalstar&FORMAT=tle", "../data/sats/tle/globalstar.tle")
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=gps-ops&FORMAT=tle", "../data/sats/tle/gps-ops.tle")
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=intelsat&FORMAT=tle", "../data/sats/tle/intelsat.tle")
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=iridium&FORMAT=tle", "../data/sats/tle/iridium.tle")
    download("https://celestrak.org/NORAD/elements/gp.php?GROUP=starlink&FORMAT=tle", "../data/sats/tle/starlink.tle")
end

function load_sats_at(tle_filenames::Vector{String}, time::DateTime)::DataFrame
    output = DataFrame()

    for tle_filename in tle_filenames
        eop_iau1980 = fetch_iers_eop()
        tles = read_tles_from_file(tle_filename)
        propagator = sgp4_init.(tles)

        dt = (t -> t.value).(time - tle_epoch.(DateTime, tles)) / 1000 / 60
        eci_2_ecef = r_eci_to_ecef(TEME(), ITRF(), datetime2julian(time), eop_iau1980)
        eci_sat_positions = (p -> p[1]).(sgp4!.(propagator, dt))
        pos = map((p -> eci_2_ecef * p), eci_sat_positions)

        append!(output, DataFrame(
            name=(t -> t.name).(tles),
            x=(x -> x[1]).(pos),
            y=(y -> y[2]).(pos),
            z=(z -> z[3]).(pos)
        ))
    end

    return output
end

function load_sats_at_now(tle_filenames::Vector{String})::DataFrame
    return load_sats_at(tle_filenames, Dates.now())
end

end

