#!/usr/bin/env julia

include("TLEs.jl")
include("Connections.jl")
include("Stations.jl")

using .TLEs
using .Connections
using .Stations

using Graphs
using SimpleWeightedGraphs

using BenchmarkTools

print("load_sats: ")
@btime load_sats_at_now(["../data/sats/tle/starlink.tle"])
sats = load_sats_at_now(["../data/sats/tle/starlink.tle"])

print("make_conections: ")
@btime get_connections(sats, 3)
connections = get_connections(sats, 3)

print("make_graph: ")
@btime create_graph(connections)
g = create_graph(connections)

print("find_path: ")
@btime a_star(g, 1000, 2000)
println("")
