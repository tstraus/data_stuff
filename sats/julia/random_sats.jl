#!/usr/bin/env julia

include("TLEs.jl")
include("Connections.jl")
include("Plot.jl")

using .TLEs
using .Connections
using .Plot

using Random
using Graphs
using SimpleWeightedGraphs

sats = load_sats_at_now(["../data/sats/tle/starlink.tle"])
#sats = load_sats_at_now(["../data/sats/tle/active.tle"])

connections = get_connections(sats, 3)
g = create_graph(connections)

source = 0
destination = 0
path = Vector{SimpleWeightedEdge{Int64,Float64}}()
while true
    global source = rand(1:length(sats.name))
    global destination = rand(1:length(sats.name))
    global path = a_star(g, source, destination)

    if length(path) > 0
        break
    end

    #println("recalculating")
end

println("$(sats.name[source]) -> $(sats.name[destination]):")
println("    direct: $(trunc(Int, get_range(sats, source, destination))) km")
println("    path: $(trunc(Int, path_length(sats, path))) km")

plot_sats(sats, connections, path)
