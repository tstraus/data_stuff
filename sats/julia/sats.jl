#!/usr/bin/env julia

include("TLEs.jl")
include("Connections.jl")
include("Stations.jl")
include("Latency.jl")
include("Plot.jl")

using .TLEs
using .Connections
using .Stations
using .Latency
using .Plot

using Random
using Graphs
using SimpleWeightedGraphs

sats = load_sats_at_now(["../data/sats/tle/starlink.tle"])
#sats = load_sats_at_now(["../data/sats/tle/active.tle"])

transmitter = get_closest_sat(get_transmitter(), sats)
receiver = get_closest_sat(get_receiver(), sats)

connections = get_connections(sats, 3)
g = create_graph(connections)
path = a_star(g, transmitter[2], receiver[2])
travel_length = path_length(sats, path) + transmitter[1] + receiver[1]
latency = get_avg_latency_ms(travel_length, length(path), 100000)

println("Eiffel Tower to Arecibo Observatory via...")
println("$(sats.name[transmitter[2]]) -> $(sats.name[receiver[2]]):")
println("    direct: $(trunc(Int, get_range(get_transmitter(), get_receiver()))) km")
println("    path: $(trunc(Int, travel_length)) km")
println("    hops in path: $(length(path)) sats")
println("    avg latency: $(round(latency[1], digits=3)) ms")
println("    packet loss: $(round(latency[2], digits=3)) %")

plot_sats(sats, connections, path, get_transmitter(), get_receiver())

