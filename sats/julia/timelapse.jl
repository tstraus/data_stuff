#!/usr/bin/env julia

include("TLEs.jl")
include("Connections.jl")
include("Stations.jl")
include("Latency.jl")

using .TLEs
using .Connections
using .Stations
using .Latency

using Statistics
using Dates
using Random
using Graphs
using SimpleWeightedGraphs
using UnicodePlots

function get_first_element(station_data)
	return station_data[1]
end

function get_second_element(station_data)
	return station_data[2]
end

# Ref() guides the broadcast operator to the desired variable
# the non Ref'd one

times = Dates.now() + Dates.Minute.(collect(0:96) * 15) # 24 hours in 15 minute increments
#times = Dates.now() + Dates.Minute.(collect(0:60)) # 1 hour in 1 minute increments
#times = Dates.now() + Dates.Second.(collect(0:60) * 5) # 5 minutes in 5 second increments
#times = times[1:4] # for quicker testing

sats = load_sats_at.(Ref(["../data/sats/tle/starlink.tle"]), times)

transmitter = get_closest_sat.(Ref(get_transmitter()), sats)
transmitter_range = get_first_element.(transmitter)
transmitter_get_second_element = get_second_element.(transmitter)
receiver = get_closest_sat.(Ref(get_receiver()), sats)
receiver_range = get_first_element.(receiver)
receiver_get_second_element = get_second_element.(receiver)

direct_length = get_range(get_transmitter(), get_receiver())

connections = get_connections.(sats, 3)
g = create_graph.(connections)
path = a_star.(g, transmitter_get_second_element, receiver_get_second_element)
travel_length = path_length.(sats, path) .+ transmitter_range .+ receiver_range
latency = get_avg_latency_ms.(travel_length, length.(path), 100000)
avg_latency = get_first_element.(latency)
packet_loss = get_second_element.(latency)

display(lineplot(times, travel_length, title="Path Length (km)", width=140))
display(lineplot(times, length.(path), title="Numer of Sats in Path", width=140))
display(lineplot(times, avg_latency, title="Average Latency (ms)", width=140))
display(lineplot(times, packet_loss, title="Packet Loss (%)", width=140))

println("\nEiffel Tower to Arecibo Observatory over the next 24 Hours")
#println("\nEiffel Tower to Arecibo Observatory over the next Hour")
#println("\nEiffel Tower to Arecibo Observatory over the next 5 Minutes")
println("    direct: $(trunc(Int, direct_length)) km")
println("    path: $(round(mean(filter(x -> x != Inf, travel_length)), digits=3)) km")
println("    hops in path: $(round(mean(filter(x -> x != 0, length.(path))), digits=3)) sats")
println("    avg latency: $(round(mean(filter(x -> x != Inf, avg_latency)), digits=3)) ms")
println("    packet loss (connected): $(round(mean(filter(x -> x != Inf, packet_loss)), digits=3)) %")
println("    packet loss (total): $(round(mean(replace(packet_loss, Inf=>100)), digits=3)) %")
