#!/usr/bin/env python3

import sats
import math
import json
import numpy as np
from skyfield.api import wgs84


B = 1.38 * 10**-23  # boltzman constant

speeds_in_media = {  # meters / second
    "vacuum": 299792458,
    "air": 299792458 / 1.000293,
    "copper": 2.3e8,
    "fiber": 299792458 / 1.47,
}
# print(speeds_in_media)


def miles_to_meters(miles: float) -> float:
    return miles * 1609.34


# dBW -> W
def dBW_to_Watt(p_dBW: float) -> float:
    return 10 ** (p_dBW / 10)


# W -> dBW
def to_dB(num: float) -> float:
    return 10 * math.log10(num)


def getPowerAtReceiver(frequency: float, power: float, distance: float) -> float:
    power_density = power / (distance * 4 * math.pi)
    wavelength = speeds_in_media["vacuum"] / frequency
    p_db = (
        10 * math.log10(power_density)
        + 20 * math.log10(wavelength)
        - 10 * math.log10(4 * math.pi)
    )
    return p_db


def viridis(value: float, min: float, max: float) -> str:
    x = (value - min) / (max - min)
    x = np.clip(x, 0.0, 1.0)
    x1 = np.array([1.0, x, x * x, x * x * x])
    x2 = x1 * x1[3] * x
    vec = np.array(
        [
            np.dot(x1, np.array([+0.280268003, -0.143510503, +2.225793877, -14.815088879])) + np.dot(np.array([x2[0], x2[1]]), np.array([+25.212752309, -11.772589584])),
            np.dot(x1, np.array([-0.002117546, +1.617109353, -1.909305070, +2.701152864])) + np.dot(np.array([x2[0], x2[1]]), np.array([-1.685288385, +0.178738871])),
            np.dot(x1, np.array([+0.300805501, +2.614650302, -12.019139090, +28.933559110])) + np.dot(np.array([x2[0], x2[1]]), np.array([-33.491294770, +13.762053843])),
        ]
    )
    vec = np.clip(vec, 0.0, 1.0)
    return f"rgb({int(vec[0] * 255)}, {int(vec[1] * 255)}, {int(vec[2] * 255)})"


def inferno(value: float, min: float, max: float) -> str:
    x = (value - min) / (max - min)
    x = np.clip(x, 0.0, 1.0)
    x1 = np.array([1.0, x, x * x, x * x * x])
    x2 = x1 * x1[3] * x
    vec = np.array(
        [
            np.dot(x1, np.array([-0.027780558, +1.228188385, +0.278906882, +3.892783760])) + np.dot(np.array([x2[0], x2[1]]), np.array([-8.490712758, +4.069046086])),
            np.dot(x1, np.array([+0.014065206, +0.015360518, +1.605395918, -4.821108251])) + np.dot(np.array([x2[0], x2[1]]), np.array([+8.389314011, -4.193858954])),
            np.dot(x1, np.array([-0.019628385, +3.122510347, -5.893222355, +2.798380308])) + np.dot(np.array([x2[0], x2[1]]), np.array([-3.608884658, +4.324996022])),
        ]
    )
    vec = np.clip(vec, 0.0, 1.0)
    return f"rgb({int(vec[0] * 255)}, {int(vec[1] * 255)}, {int(vec[2] * 255)})"


link_types = {
    "u1": {
        "name": "uplink 1",
        "media": "air",
        "channels": [
            {
                "frequency_center": 3.750,  # MHz
                "frequency_bandwidth": 0.500,  # MHz
            }
        ],
        "pcr": {
            "datarate": 10000,  # bits / second
            "packet_size": 128 * 8,  # bits
            "curve": [
                {"sinr": 0.0, "por": 0.0},  # dB, %
                {"sinr": 20.0, "por": 1.0},
            ],
        },
    },
    "gps": {
        "name": "GPS Navigation Messages",
        "media": "air",
        "channels": [
            {
                "frequency_center": 1000,  # MHz
                "frequency_bandwidth": 3,  # MHz
            }
        ],
        "pcr": {
            "datarate": 50,  # bits / second
            "packet_size": 300,  # bits
            "curve": [
                {"sinr": -6.0, "por": 0},
                {"sinr": -5.0, "por": 1.4},
                {"sinr": -4.0, "por": 20.6},
                {"sinr": -3.0, "por": 63.1},
                {"sinr": -2.0, "por": 90.5},
                {"sinr": -1.0, "por": 98.5},
                {"sinr": 0.0, "por": 99.9},
                {"sinr": 1.0, "por": 100.0},
            ],
        },
    },
}
# print(link_types)

radio_types = {
    "r1": {
        "name": "radio 1",
        "links": {"u1": {"tx": True, "rx": False, "power": 25}},  # W
        "antenna": "isotropic",
        "noise_figure": 4,  # dBW
    },
    "gps_tx": {
        "name": "GPS Transmitter",
        "links": {"gps": {"tx": True, "rx": False, "power": 25}},  # W
        "antenna": "isotropic",
        "noise_figure": 4,  # dBW
    },
    "gps_rx": {
        "name": "GPS Receiver",
        "links": {"gps": {"tx": False, "rx": True, "power": 0}},  # W
        "antenna": "isotropic",
        "noise_figure": 4,  # dBW
    },
}
# print(radio_types)

jammer_types = {
    "gps_j": {
        "name": "GPS Jammer",
        "frequency_center": 1000,  # MHz
        "frequency_bandwidth": 3,  # MHz
        "power": 20,  # W
    }
}

scenario = {
    "radios": [
        {
            "name": "A GPS Sat",
            "type": "gps_tx",
            "pos": np.array(wgs84.latlon(0, 35, 20200 * 1000).itrs_xyz.m),
            "temp": 200,  # K
        },
        {
            "name": "A Ground GPS Receiver",
            "type": "gps_rx",
            "pos": np.array(wgs84.latlon(-0.5, -0.5, 0).itrs_xyz.m),
            "temp": 290,
        },
    ],
    "jammers": [
        {
            "name": "A GPS Jammer",
            "type": "gps_j",
            "pos": np.array(wgs84.latlon(2.5, 5.5, 10000).itrs_xyz.m),
        },
        {
            "name": "B GPS Jammer",
            "type": "gps_j",
            "pos": np.array(wgs84.latlon(-3.5, 5.5, 12000).itrs_xyz.m),
        }
    ],
}


def snr(tx: int, rx: int, link_type: str, channel: int = 0) -> float:
    link = link_types[link_type]
    tx_radio = scenario["radios"][tx]
    tx_radio_type = radio_types[tx_radio["type"]]
    rx_radio = scenario["radios"][rx]
    rx_radio_type = radio_types[rx_radio["type"]]

    s_power = tx_radio_type["links"][link_type]["power"]
    s_distance = np.linalg.norm(tx_radio["pos"] - rx_radio["pos"])

    frequency = link["channels"][channel]["frequency_center"] * 10**6  # convert to Hz
    bandwidth = (
        link["channels"][channel]["frequency_bandwidth"] * 10**6
    )  # convert to Hz

    T = rx_radio["temp"]
    min_input_noise = to_dB(B * T)

    NF = rx_radio_type["noise_figure"]
    internal_noise_floor = min_input_noise + NF + to_dB(bandwidth)

    s_db = getPowerAtReceiver(frequency, s_power, s_distance)  # signal power received
    # j_db = -64.51426127956051
    # j_db = -60 # a very small number
    j_db = -100
    for jam in scenario["jammers"]:
        jam_type = jammer_types[jam["type"]]
        j_frequency = jam_type["frequency_center"]
        j_bandwidth = jam_type["frequency_bandwidth"]
        j_power = jam_type["power"]
        j_distance = np.linalg.norm(jam["pos"] - rx_radio["pos"])
        j_db = j_db + getPowerAtReceiver(j_frequency, j_power, j_distance)


    total_noise = dBW_to_Watt(internal_noise_floor) + dBW_to_Watt(j_db)
    total_noise_dBW = to_dB(total_noise)
    snjr = s_db - total_noise_dBW
    #j_over_s = -snjr

    # print(f"min_input_noise: {min_input_noise} dB")
    # print(f"internal_noise_floor: {internal_noise_floor} dB")
    # print(f"signal: {dBW_to_Watt(s_db)} W")
    # print(f"noise: {total_noise} W")
    # print(f"SNJR: {snjr} dB")
    # print(f"J/S: {j_over_s} dB")

    return snjr


print(f"SNR: {snr(0, 1, 'gps')} dB")


# plot the base scenario
link = "gps"
fig = sats.get_earth_plot()

for rad in scenario["radios"]:
    rl = radio_types[rad["type"]]["links"][link]
    color = "white"
    if rl["tx"] == True and rl["rx"] == False:
        color = "green"
    elif rl["tx"] == False and rl["rx"] == True:
        color = "blue"
    elif rl["tx"] == True and rl["rx"] == True:
        color = "aqua"

    fig = sats.plot_points(
        [float(rad["pos"][0])],
        [float(rad["pos"][1])],
        [float(rad["pos"][2])],
        fig, color
    )

for jam in scenario["jammers"]:
    color = "red"
    fig = sats.plot_points(
        [float(jam["pos"][0])],
        [float(jam["pos"][1])],
        [float(jam["pos"][2])],
        fig, color
    )


#fig = sats.plot_points(
#    [float(scenario["radios"][0]["pos"][0])],
#    [float(scenario["radios"][0]["pos"][1])],
#    [float(scenario["radios"][0]["pos"][2])],
#    fig,
#    "cyan",
#)
#print(scenario)


scenario["radios"].pop()  # remove the receiver

x = []
y = []
z = []

# add a bunch of receivers
for lat in range(-60, 60):
    for lon in range(-90, 90):
        pos = wgs84.latlon(lat, lon, 2000).itrs_xyz.m
        x.append(float(pos[0]))  # for plots
        y.append(float(pos[1]))  # for plots
        z.append(float(pos[2]))  # for plots

        scenario["radios"].append(
            {
                "name": "Ground GPS Receiver",
                "type": "gps_rx",
                "pos": np.array(pos),
                "temp": 290,
            }
        )

# calculate SNR and track the min and max
min = 100000
max = -100000
snrs = []
for i in range(1, len(scenario["radios"])):
    s = snr(0, i, "gps")
    s = -s # invert to visualize jamming hotspots
    if s < min:
        min = s
    if s > max:
        max = s
    snrs.append(s)

# calculate the colors to display them as
colors = []
for s in snrs:
    #colors.append(viridis(s, min, max))
    colors.append(inferno(s, min, max))

# print(snrs)
#print(f"min: {min} dB, max: {max} dB")

fig = sats.plot_points(x, y, z, fig, colors)
fig.show()
