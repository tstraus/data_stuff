#!/usr/bin/env python3

import pandas as pd
import numpy as np
import sats as s


def normalize(arr):
    #norm_arr = (arr - np.min(arr)) / (np.max(arr) - np.min(arr))
    tol = 0.0001
    m = np.sqrt(
		arr[0] * arr[0] + 
		arr[1] * arr[1] + 
		arr[2] * arr[2]
	)
    
    if m <= tol:
        m = 1
        
    norm_arr = arr
    norm_arr[0] = norm_arr[0] / m
    norm_arr[1] = norm_arr[1] / m
    norm_arr[2] = norm_arr[2] / m

    if (np.abs(norm_arr[0]) < tol):
        norm_arr[0] = 0
    if (np.abs(norm_arr[1]) < tol):
        norm_arr[1] = 0
    if (np.abs(norm_arr[2]) < tol):
        norm_arr[2] = 0
    
    return norm_arr


def vector_rejection(a, b):
    return a - (((np.dot(a, b)) / (np.dot(b, b))) * b)


def get_in_plane_idx(sats, subject_idx, threshold=0.08):
    # a_idx = 1000
    a_idx = subject_idx
    a_pos = np.array([sats["pos_x"][a_idx], sats["pos_y"][a_idx], sats["pos_z"][a_idx]])
    a_vel = np.array([sats["vel_x"][a_idx], sats["vel_y"][a_idx], sats["vel_z"][a_idx]])
    a_plane = normalize(np.cross(a_pos, a_vel))
    a_pos = normalize(a_pos)
    a_vel = normalize(a_vel)

    b_pos = []
    b_vel = []
    for i in range(0, sats.pos_x.size):
        pos = np.array([sats["pos_x"][i], sats["pos_y"][i], sats["pos_z"][i]])
        b_pos.append(normalize(pos))
        vel = np.array([sats["vel_x"][i], sats["vel_y"][i], sats["vel_z"][i]])
        b_vel.append(normalize(vel))

    pos_plane = np.array([])
    vel_plane = np.array([])
    for i in range(0, sats.pos_x.size):
        if i != a_idx:
            pos_plane = np.append(pos_plane, np.abs(np.dot(normalize(b_pos[i]), a_plane)))
            vel_plane = np.append(vel_plane, np.abs(np.dot(normalize(b_vel[i]), a_plane)))
        else:
            pos_plane = np.append(pos_plane, np.nan)
            vel_plane = np.append(vel_plane, np.nan)

    in_plane_idx = []
    in_plane_angle = []
    for i in range(0, sats.pos_x.size):
        if (
            pos_plane[i] != np.nan
            and vel_plane[i] != np.nan
            and pos_plane[i] <= threshold
            and vel_plane[i] <= threshold
        ):
            in_plane_idx.append(i)

            t = vector_rejection(b_pos[i], a_plane)
            s = vector_rejection(a_pos, a_plane)
            dp = np.dot(s, t)
            det = np.dot(a_plane, np.cross(s, t)) # determinant

            in_plane_angle.append(np.arctan2(det, dp))

    return in_plane_idx, in_plane_angle


sats = s.load_sats_at_now("../data/sats/tle/starlink.tle")
# print(sats)

subject_idx = 1000
in_plane_idx, in_plane_angle = get_in_plane_idx(sats, subject_idx, 0.07)
# print(in_plane_idx)

print(sats.name[subject_idx])
print("---------------")
for i in range(0, len(in_plane_idx)):
    print(f"{sats.name[in_plane_idx[i]]}: {in_plane_angle[i]} rad")
    
in_plane_idx.append(subject_idx)
#print(sats.loc[in_plane_idx])
    
s.plot_connections_and_path(sats.loc[in_plane_idx], pd.DataFrame(), [])

# ecef pos cross velocity for sat gives an orbital plane
# normalize it
# normalized other sat pos and vel dot that plane checks how close to same plane
# currently threshold is 0.08
#
# angle between two on plane.
# angle from sat a = atan2(a dot(a plane cross b plane), a plane dot b plane)
