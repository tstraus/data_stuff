#!/usr/bin/env python3

import random
import numpy as np
import networkx as nx
import sats as s

#pio.renderers.default = "chromium"

sats = s.load_sats_at_now("../data/sats/tle/starlink.tle")
connections = s.get_connections(sats, 3)
g = s.get_graph(sats, connections)

begin = 0
end = 0
while True:
    begin = random.randint(0, len(sats))
    end = random.randint(0, len(sats))
    if nx.has_path(g, begin, end):
        break

path = np.array(
    nx.astar_path(g, source=begin, target=end, weight="length"), dtype=np.int32
)
direct = s.get_range(sats, begin, end) / 1000.0
travel_length = s.get_path_length(sats, path) / 1000.0
latency = s.get_avg_latency_ms(travel_length, len(path), 100000)

print(f"{sats.name[begin]} -> {sats.name[end]}:")
print(f"    direct: {int(direct)} km")
print(f"    path: {int(travel_length)} km")
print(f"    hops in path: {len(path)} sats")
print(f"    avg latency: {round(latency['mean'], 3)} ms")
print(f"    packet loss: {round(latency['drop_rate'], 3)} %")
s.plot_connections_and_path(sats, connections, path)
