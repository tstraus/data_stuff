#!/usr/bin/env python3

import sys
import pandas as pd
import numpy as np
import networkx as nx

import plotly.io as pio
import plotly.express as px
import plotly.graph_objects as go
from skyfield.api import load, wgs84
from skyfield.framelib import itrs
import geopandas as gpd
import plotly.graph_objects as go


if sys.platform == "linux":
    pio.renderers.default = "chromium"
#pio.renderers.default = "chrome"


def load_sats_at_now(tle_filename: str) -> pd.DataFrame:
    sats = load.tle_file(tle_filename)

    # get current time
    ts = load.timescale()
    t = ts.now()

    sat_pos = []

    for sat in sats:
        # print(f"loaded: {sat}")
        geocentric = sat.at(t)  # calculate where it is now
        # pos = wgs84.geographic_position_of(geocentric) # calculate lat lon alt
        ecef_pos, ecef_vel = geocentric.frame_xyz_and_velocity(
            itrs
        )  # convert ECI to ECEF
        ecef_pos = ecef_pos.m
        ecef_vel = ecef_vel.m_per_s
        sat_pos.append(
            {
                "name": sat.name,
                "pos_x": ecef_pos[0],
                "pos_y": ecef_pos[1],
                "pos_z": ecef_pos[2],
                "vel_x": ecef_vel[0],
                "vel_y": ecef_vel[1],
                "vel_z": ecef_vel[2],
            }
        )

    # return pd.DataFrame(sat_pos).head(10)
    return pd.DataFrame(sat_pos)


def get_range(sats: pd.DataFrame, a_index: int, b_index: int) -> float:
    diff_x = sats.pos_x[a_index] - sats.pos_x[b_index]
    diff_y = sats.pos_y[a_index] - sats.pos_y[b_index]
    diff_z = sats.pos_z[a_index] - sats.pos_z[b_index]
    return np.sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z)


def get_n_closest(sats: pd.DataFrame, sat_index: int, num: int = 1) -> pd.DataFrame:
    diff_x = sats.pos_x[sat_index] - sats.pos_x
    diff_y = sats.pos_y[sat_index] - sats.pos_y
    diff_z = sats.pos_z[sat_index] - sats.pos_z

    diff = np.sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z).sort_values()

    output = pd.DataFrame()
    output["sat_index"] = diff.index.to_numpy()[1 : 1 + num]
    output["range"] = diff.to_numpy()[1 : 1 + num]

    return output


def print_closest(sats: pd.DataFrame, num: int = 1):
    for i, _ in sats.iterrows():
        print(f"{sats.name[i]} ->")

        closest = get_n_closest(sats, i, num)

        for _, row in closest.iterrows():
            print(f"    {sats.name[row.sat_index]}: {row.range / 1000.0} km")


def get_connections(sats: pd.DataFrame, num: int) -> pd.DataFrame:
    connections = []

    for i in range(0, len(sats)):
        closest = get_n_closest(sats, i, num)
        for _, row in closest.iterrows():
            connections.append(
                {"from_sat": i, "to_sat": np.int64(row.sat_index), "range": row.range}
            )

    return pd.DataFrame(connections)


def get_graph(sats: pd.DataFrame, connections: pd.DataFrame) -> nx.Graph:
    g = nx.Graph()

    for i, sat in sats.iterrows():
        # g.add_node(i, pos=(sat.pos_x, sat.pos_y, sat.pos_z))
        g.add_node(i)

    for i, conn in connections.iterrows():
        g.add_edge(conn.from_sat, conn.to_sat, length=conn.range)

    return g


def get_path_length(sats: pd.DataFrame, path: list[int]) -> float:
    length = 0.0

    for i in range(1, len(path)):
        length += get_range(sats, path[i - 1], path[i])

    return length


def get_latency_us(mean_us: float, min_us: float, n: int):
    return np.random.gamma(2, 2, n) * (mean_us / 8) + min_us


def get_path_latency_ms(distance_km: float, num_sats: int) -> float:
    min_latency = 250.0
    mean_latency = 500.0
    drop_latency = mean_latency * 3.7

    latencies = get_latency_us(mean_latency, min_latency, num_sats)

    if np.max(latencies) > drop_latency:
        return -1.0

    sat_latency = np.sum(latencies) * 0.001  # us -> ms

    return sat_latency + (distance_km / 299.792458)  # speed of light in km / ms


def get_avg_latency_ms(distance_km: float, num_sats: int, samples: int) -> dict:
    if num_sats <= 0:
        return dict(mean=np.inf, drop_rate=np.inf)

    latencies = []
    drop_count = 0
    for sample in range(samples):
        result = get_path_latency_ms(distance_km, num_sats)
        if result == -1:
            drop_count = drop_count + 1
        else:
            latencies.append(result)

    return dict(mean=np.mean(latencies), drop_rate=(drop_count / samples * 100.0))


def make_sphere_vertices(radius: float, segments: int) -> pd.DataFrame:
    N = segments  # Sphere resolution (both rings and segments, can be separated to different constants)
    theta, z = np.meshgrid(np.linspace(-np.pi, np.pi, N), np.linspace(-1, 1, N))
    r = np.sqrt(1 - z**2)
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    x = x.ravel()
    y = y.ravel()
    z = z.ravel()

    x = radius * x
    y = radius * y
    z = radius * z

    output = pd.DataFrame()
    output["x"] = x
    output["y"] = y
    output["z"] = z
    return output


def make_sphere_indices(segments: int) -> pd.DataFrame:
    N = segments  # Sphere resolution (both rings and segments, can be separated to different constants)
    indices = np.arange(N * (N - 1) - 1)
    i1 = np.concatenate([indices, (indices // N + 1) * N + (indices + 1) % N])
    i2 = np.concatenate([indices + N, indices // N * N + (indices + 1) % N])
    i3 = np.concatenate([(indices // N + 1) * N + (indices + 1) % N, indices])

    output = pd.DataFrame()
    output["i"] = i1
    output["j"] = i2
    output["k"] = i3

    return output


def plot_polygon(poly):
    xy_coords = poly.exterior.coords.xy
    lon = np.array(xy_coords[0])
    lat = np.array(xy_coords[1])
    
    lon = lon * np.pi/180
    lat = lat * np.pi/180
    
    R = 6378.134 * 1000
    x = R * np.cos(lat) * np.cos(lon)
    y = R * np.cos(lat) * np.sin(lon)
    z = R * np.sin(lat)
    
    return x, y, z


def add_countries(fig: go.Figure, filename: str = "../data/countries_simple.geojson") -> go.Figure:
    gdf = gpd.read_file(filename)
    for i in gdf.index:
        # print(gdf.loc[i].NAME)            # Call a specific attribute
        polys = gdf.loc[i].geometry         # Polygons or MultiPolygons 
    
        if polys.geom_type == 'Polygon':
            x, y, z = plot_polygon(polys)
            fig.add_trace(go.Scatter3d(x=x, y=y, z=z, mode='lines', line=dict(color=f'white'), showlegend=False) ) 

        elif polys.geom_type == 'MultiPolygon':

            for poly in polys.geoms:
                x, y, z = plot_polygon(poly)
                fig.add_trace(go.Scatter3d(x=x, y=y, z=z, mode='lines', line=dict(color=f'white'), showlegend=False) ) 

    return fig


def get_earth_plot(title: str = "") -> go.Figure:
    n = 256
    sphere_i = make_sphere_vertices(6378.14 * 1000, n)
    sphere_v = make_sphere_indices(n)

    fig = go.Figure(data=go.Mesh3d(
        x=sphere_i.x,
        y=sphere_i.y,
        z=sphere_i.z,
        i=sphere_v.i,
        j=sphere_v.j,
        k=sphere_v.k,
        color="darkslategray",
        showlegend=False,
    ))

    fig = add_countries(fig)

    fig.update_layout(title=dict(text=title), template="plotly_dark")

    return fig


def plot_points(x, y, z, fig: go.Figure = get_earth_plot(), color: str = "red", size: int = 7):
    fig.add_trace(
        go.Scatter3d(
            x=x,
            y=y,
            z=z,
            mode="markers",
            marker=dict(color=color, size=size),
            showlegend=False
        )
    )

    return fig


def plot_connections_and_path(
    sats: pd.DataFrame, connections: pd.DataFrame, path: list[int]
):
    sats["order"] = sats.index
    sats["size"] = 7

    fig = px.scatter_3d(
        sats,
        x="pos_x",
        y="pos_y",
        z="pos_z",
        size="size",
        opacity=1.0,
        template="plotly_dark",
        color="order",
        color_continuous_scale="viridis",
    )

    fig = add_countries(fig)

    sats["pos_x_r"] = sats.pos_x.round(4)
    sats["pos_y_r"] = sats.pos_y.round(4)
    sats["pos_z_r"] = sats.pos_z.round(4)

    cons_x = []
    cons_y = []
    cons_z = []

    for row in connections.itertuples():
        f = row.from_sat
        t = row.to_sat

        cons_x.extend([sats.pos_x[f], sats.pos_x[t], None])
        cons_y.extend([sats.pos_y[f], sats.pos_y[t], None])
        cons_z.extend([sats.pos_z[f], sats.pos_z[t], None])

    fig.add_trace(
        go.Scatter3d(
            x=cons_x,
            y=cons_y,
            z=cons_z,
            mode="lines",
            marker=dict(color="darksalmon"),
            showlegend=False,
        )
    )

    path_x = []
    path_y = []
    path_z = []

    for p in path:
        path_x.append(sats.pos_x[p])
        path_y.append(sats.pos_y[p])
        path_z.append(sats.pos_z[p])

    fig.add_trace(
        go.Scatter3d(
            x=path_x,
            y=path_y,
            z=path_z,
            mode="lines",
            marker=dict(color="red"),
            line=dict(width=5),
            showlegend=False,
        )
    )

    n = 128
    sphere_i = make_sphere_vertices(6378.14 * 1000, n)
    sphere_v = make_sphere_indices(n)

    fig.add_mesh3d(
        x=sphere_i.x,
        y=sphere_i.y,
        z=sphere_i.z,
        i=sphere_v.i,
        j=sphere_v.j,
        k=sphere_v.k,
        color="darkslategray",
        showlegend=False,
    )

    fig.show()
