#!/usr/bin/env python3

import math

# https://rahsoft.com/2021/06/29/example-questions-part-1/
# https://www.electronics-notes.com/articles/radio/radio-receiver-sensitivity/what-is-noise-floor.php
# https://www.cyntony.com/blog/how-much-j-to-s-do-you-really-need-for-jamming-communications

# constants
c = 299792458  # speed of light (m/s)
b = 1.38 * 10**-23  # boltman constant


def miles_to_meters(miles):
    return miles * 1609.34


def getPowerAtReceiver(frequency, power, distance):
    power_density = power / (distance * 4 * math.pi)
    wavelength = c / frequency
    p_db = (
        10 * math.log10(power_density)
        + 20 * math.log10(wavelength)
        - 10 * math.log10(4 * math.pi)
    )
    return p_db


def dBW_to_Watt(p_dBW):
    return 10 ** (p_dBW / 10)


def to_dB(num):
    return 10 * math.log10(num)


# GPS
s_power = 25  # signal power (W)
s_distance = miles_to_meters(miles=12550)  # distance from receiver
# Made up Jammer
j_power = 200  # jammer power
j_distance = miles_to_meters(miles=200)  # distance of jammer from receiver

# distance = 482803
frequency = 1 * 10**9  # 1 GHz
bandwidth = 10 * math.log10(3 * 10**6)  # 3 MHz

T = 290  # temperature (K))
min_input_noise = 10 * math.log10(b * T)  # dBW
print("min_input_noise: ", min_input_noise)

NF = 4  # noise figure (dBW)
internal_noise_floor = min_input_noise + bandwidth + NF

s_db = getPowerAtReceiver(frequency, s_power, s_distance)  # signal power received
j_db = getPowerAtReceiver(frequency, j_power, j_distance)  # signal power received
print("s: ", dBW_to_Watt(s_db))
print("j: ", dBW_to_Watt(j_db))

# snr without jammer noise
# output = s_db - internal_noise_floor# - j_db

# snr with internal and jammer noise
total_noise = dBW_to_Watt(internal_noise_floor) + dBW_to_Watt(j_db)
total_noise_dbW = to_dB(total_noise)
snjr = s_db - total_noise_dbW
j_over_s = -snjr

print("internal_noise_floor: ", internal_noise_floor)
print("s_db: ", s_db)
print("j_db: ", j_db)
# print("output: ", output)
print("SNJR: ", snjr)
print("J/S: ", j_over_s)
