#!/usr/bin/env python3

import requests
from rich.progress import track


tles = [
    "active",
    "globalstar",
    "gnss",
    "galileo",
    "beidou",
    "gps-ops",
    "weather",
    "noaa",
    "goes",
    "intelsat",
    "iridium",
    "starlink"
]

for tle in track(tles, description="updating TLEs"):
    response = requests.get(f"https://celestrak.org/NORAD/elements/gp.php?GROUP={tle}&FORMAT=tle")

    with open(f"../data/sats/tle/{tle}.tle", "wb") as output:
        output.write(response.content)
