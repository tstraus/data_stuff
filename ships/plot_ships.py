#!/usr/bin/env python3

import geopandas as gpd
import matplotlib.pyplot as plt
import geoplot as gplt
import geoplot.crs as gcrs

gdf = gpd.read_parquet("./ships.parquet")
# print(gdf)

# gplt.pointplot(gdf)

world = gpd.read_file("../data/countries.geojson")
# world = gpd.read_file(gplt.datasets.get_path("world"))
# print(world)
# ax = gplt.polyplot(world, projection=gcrs.AlbersEqualArea())
ax = gplt.polyplot(world, projection=gcrs.WebMercator())
ax = gplt.kdeplot(gdf, ax=ax, cmap="viridis", projection=gcrs.WebMercator(), alpha=0.5)
gplt.pointplot(gdf, ax=ax)
plt.show()
