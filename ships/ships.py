#!/usr/bin/env python3

import rasterio
import numpy as np
import pandas as pd
import geopandas as gpd
#import matplotlib.pyplot as plt

# ship average cruising speed
# container: 12 - 22 knots
# tanker: 12 - 17 knots
# cruise: 20 - 25 knots
# roro: 16 - 22 knots
# bulk carrier: 12 - 23 knots
# fishing: 2 - 10 knots
# these should should be lower in extremely dense pixels

# data point units
# "how many ships spent an hour in the pixel over the span of a month"

def haversine(lon1, lat1, lon2, lat2):
    R = 6371
    # km
    dLat = np.radians(lat2 - lat1)
    dLon = np.radians(lon2 - lon1)
    a = np.sin(dLat / 2) * np.sin(dLat / 2) + np.cos(np.radians(lat1)) * np.cos(np.radians(lat2)) * np.sin(dLon / 2) * np.sin(dLon / 2)
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    d = R * c

    return d


def pixel_area(top, bottom, left, right, bl_tr_diag):
    s1 = 0.5 * (top + left + bl_tr_diag)
    a1 = np.sqrt(s1 * (s1 - top) * (s1 - left) * (s1 - bl_tr_diag))
    s2 = 0.5 * (bottom + right + bl_tr_diag)
    a2 = np.sqrt(s2 * (s2 - bottom) * (s2 - right) * (s2 - bl_tr_diag))

    return a1 + a2


with rasterio.open("./ship_density.tif") as dataset:
    data = dataset.read(1)

    #cleaned = data[data != 0]
    #print(f"min: {np.min(cleaned)}")
    #print(f"max: {np.max(cleaned)}")
    #print(f"mean: {np.mean(cleaned)}")

    #print(f"bounds: {dataset.bounds}")
    #print(f"resolution: {dataset.res}")
    #print(f"size: {dataset.width}, {dataset.height}")

    #print(dataset.transform)
    #print(f"top left pixel: {dataset.transform * (0, 0)}")
    #print(f"bottom right pixel: {dataset.transform * (dataset.width, dataset.height)}")

    cols, rows = np.meshgrid(np.arange(dataset.width), np.arange(dataset.height))
    xs, ys = rasterio.transform.xy(dataset.transform, rows, cols, offset="ul")

    # top left of each pixel
    tl_lon = np.array(xs)
    tl_lat = np.array(ys)

    # top right of each pixel
    tr_lon = tl_lon + dataset.res[0]
    tr_lat = tl_lat

    # bottom left of each pixel
    bl_lon = tl_lon
    bl_lat = tl_lat - dataset.res[1]

    # bottom right of each pixel
    br_lon = tl_lon + dataset.res[0]
    br_lat = tl_lat - dataset.res[1]

    # the bounds of a pixel
    #print(f"({tl_lon[1000][1000]:.5f},{tl_lat[1000][1000]:.5f}), ({tr_lon[1000][1000]:.5f},{tr_lat[1000][1000]:.5f})")
    #print(f"({bl_lon[1000][1000]:.5f},{bl_lat[1000][1000]:.5f}), ({br_lon[1000][1000]:.5f},{br_lat[1000][1000]:.5f})")
    print(f"({tl_lon[0][0]:.5f}, {tl_lat[0][0]:.5f}), ({tr_lon[0][0]:.5f}, {tr_lat[0][0]:.5f})")
    print(f"({bl_lon[0][0]:.5f}, {bl_lat[0][0]:.5f}), ({br_lon[0][0]:.5f}, {br_lat[0][0]:.5f})")

    top = haversine(tl_lon, tl_lat, tr_lon, tr_lat)
    bottom = haversine(bl_lon, bl_lat, br_lon, br_lat)
    left = haversine(tl_lon, tl_lat, bl_lon, bl_lat)
    right = haversine(tr_lon, tr_lat, br_lon, br_lat)
    bl_tr_diag = haversine(bl_lon, bl_lat, tr_lon, tr_lat)
    area = pixel_area(top, bottom, left, right, bl_tr_diag)
    avg_side = (top + bottom + left + right) / 4
    avg_path = (avg_side + bl_tr_diag) / 2

    #print(f"top: {top[1000][1000]:.5f} km")
    #print(f"bottom: {bottom[1000][1000]:.5f} km")
    #print(f"left: {left[1000][1000]:.5f} km")
    #print(f"right: {right[1000][1000]:.5f} km")
    #print(f"diag: {bl_tr_diag[1000][1000]:.5f} km")
    #print(f"area: {area[1000][1000]:.5f} km^2")
    print(f"pixel avg path: {avg_path[1000][1000]:.5f} km")
    print(f"ship density: {data[1000][1000]:.5f} monthly hours per km^2")

    #vel_knots = 10
    vel_knots = np.random.uniform(0.5, 20, data.shape)
    vel_kph = vel_knots * 1.852
    avg_pixel_transit_hrs = avg_path / vel_kph
    ship_pixels_per_hr = 1 / avg_pixel_transit_hrs
    print(f"avg pixel transit time: {avg_pixel_transit_hrs[1000][1000]:.5f} hrs")
    print(f"ship pixels per hours: {ship_pixels_per_hr[1000][1000]:.5f} pixels")

    hours_per_month = 24 * 30.437 # month lengths be crazy

    average_ships_per_hr = data / hours_per_month
    print(f"ships in an hour: {average_ships_per_hr[1000][1000]:.5f}")
    ships_instantaneous = average_ships_per_hr / ship_pixels_per_hr
    print(f"ships instantaneous: {ships_instantaneous[1000][1000]:.5f}")
    print(f"total movers (estimate): {np.sum(ships_instantaneous):.5f}")

    movers = np.ceil(0.6 * np.floor(ships_instantaneous)) # "whole" ships, and bias denser areas to loitering
    leftover = (np.fmod(ships_instantaneous, 1) > np.random.random(movers.shape)).astype(dtype=np.float32) # partial chance of ship
    movers = movers + leftover
    print(f"total movers (placed): {np.sum(movers)}")
    #print(f"max ships: {np.max(movers)}")
    #print(f"max density: {np.max(data)}")

    #print(np.max(ships_instantaneous))
    loiterers = 1.4 * np.floor(ships_instantaneous)
    loiterers = np.ceil(loiterers)
    #print(np.max(loiterers))
    print(f"total loiterers: {np.sum(loiterers)}")

    ships = movers + loiterers
    print(f"total ships (placed): {np.sum(ships)}")

    output = rasterio.open(
        "./ships.tiff",
        "w",
        driver="GTiff",
        height=dataset.height,
        width=dataset.width,
        count=1,
        dtype=ships.dtype,
        crs="+proj=latlong",
        transform=dataset.transform
    )
    output.write(ships, 1)
    output.close()

    # allocate them real positions
    lons = []
    lats = []
    for y in range(0, dataset.height):
        for x in range(0, dataset.width):
            if ships[y][x] > 0:
                lons.extend(np.random.uniform(min(tl_lon[y][x], bl_lon[y][x]), max(tr_lon[y][x], br_lon[y][x]), int(ships[y][x])))
                lats.extend(np.random.uniform(min(bl_lat[y][x], br_lat[y][x]), max(tl_lat[y][x], tr_lat[y][x]), int(ships[y][x])))

    #plt.scatter(lons, lats)
    #plt.show()

    df = pd.DataFrame({"longitude": lons, "latitude": lats})
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude), crs="EPSG:4326")
    gdf.to_parquet("./ships.parquet", compression="zstd", compression_level=22)
