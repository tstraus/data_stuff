#!/usr/bin/env julia

using Parquet, DataFrames, Statistics, ArgParse

function get_stock_symbol(name)
    name = basename(name)
    return replace(name, r".parquet$"=>"")
end

function load_stock_data(filename, field, days)
    output = DataFrame(read_parquet(filename))
    if hasproperty(output, field)
        output = output[:, field]
    else
        throw(KeyError(field))
    end

    if length(output) > days
        output = output[end-days:end]
    else
        throw(BoundsError(output, days))
    end

    return output
end

function get_correlated_stocks(subject_symbol, num_days)
    data_root = "../data/parquet/stocks/stocks"
    subject_stock_file = "$data_root/$(subject_symbol[1:1])/$subject_symbol.parquet"
    
    subject = load_stock_data(subject_stock_file, "Close", num_days)
    
    # make a thread safe place to put the results
    results = Vector{Vector{Pair{String, Float64}}}()
    for _ in 1:Threads.nthreads()
        push!(results, Vector{Pair{String, Float64}}())
    end
    
    @time begin
    for (root, dirs, _) in walkdir(data_root)
        Threads.@threads for dir in dirs
            for (_, _, files) in walkdir("$root/$dir")
                for file in filter(f->endswith(f, ".parquet"), files)
                    other_stock_file = "$root/$dir/$file"
                    other_symbol = get_stock_symbol(other_stock_file)
    
                    try
                        other = load_stock_data(other_stock_file, "Close", num_days)
                        result = cor(subject, other)
                        push!(results[Threads.threadid()], other_symbol => result)
                    catch _
                    end
                end
            end
        end
    end
    
    collected = []
    for result in results
        for r in result
            push!(collected, r)
        end
    end
    sort!(collected, by=x->x.second, rev=true)
    
    answer = DataFrame(
        positive=collected[2:11], # start at 2 to skip ourselves
        negative=reverse(collected[end-9:end]) # reverse to get most negativelycorrelated
    )
    end
    
    return answer
end

options = ArgParseSettings()
options.description = "Finds the most positively and negatively correlated stocks to the given stock"
@add_arg_table! options begin
    "stock_symbol"
        help = "Symbol of the stock to compare against"
        required = true
    "days"
        help = "Days back in time to start the comparison"
        default = 365
end
args = parse_args(ARGS, options)

println(get_correlated_stocks(uppercase(args["stock_symbol"]), args["days"]))

