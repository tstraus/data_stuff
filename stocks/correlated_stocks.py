#!/usr/bin/env python3

import os
import argparse
import pandas as pd
import scipy.stats as stats
from pyarrow import parquet

parser = argparse.ArgumentParser(
    description="Finds the most positively and negatively correlated stocks to the given stock"
)
parser.add_argument(
    "stock_symbol", nargs=1, help="Symbol of the stock to compare against"
)
parser.add_argument(
    "-d", "--days", default=365, help="Days back in time to start the comparison"
)
args = parser.parse_args()


def get_stock_symbol(name):
    name = os.path.basename(name)
    return name.replace(".parquet", "")


def load_stock_data(filename, field, days):
    output = parquet.read_table(filename).to_pandas()
    if field in output:
        output = output[field]
    else:
        raise KeyError(field)

    if len(output) > days:
        output = output.tail(days)
    else:
        raise IndexError(output, days)

    return output

def get_correlated_stocks(subject_symbol, num_days):
    results = []
    data_root = "../data/parquet/stocks/stocks"
    subject_stock_file = f"{data_root}/{subject_symbol[0]}/{subject_symbol}.parquet"

    subject = load_stock_data(subject_stock_file, "Close", num_days)

    for root, _, files in os.walk(data_root):
        for file in filter(lambda f: f.endswith((".parquet")), files):
            other_stock_file = f"{root}/{file}"
            other_symbol = get_stock_symbol(other_stock_file)

            try:
                other = load_stock_data(other_stock_file, "Close", num_days)
                result = stats.pearsonr(subject, other)
                results.append([other_symbol, result.statistic])
            except:
                pass

    # convert to dataframe and sort
    results = pd.DataFrame(results, columns=["Symbol", "Correlation"])
    results.sort_values("Correlation", ascending=False, inplace=True, ignore_index=True)

    pos = results.head(11).tail(10).copy().reset_index(drop=True) # get first 10 that isn't the subject
    neg = results.tail(10).sort_values("Correlation", ignore_index=True).copy()

    output =  pd.DataFrame(data={
        "+ Symbol": pos["Symbol"],
        "+ Correlation": pos["Correlation"],
        "- Symbol": neg["Symbol"],
        "- Correlation": neg["Correlation"]
    })

    output.index += 1 # make index start at 1 for pretty print
    return output

print(get_correlated_stocks(args.stock_symbol[0].upper(), args.days))
