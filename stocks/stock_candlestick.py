#!/usr/bin/env python3

import argparse
from math import pi
from pyarrow import parquet
from bokeh.plotting import figure, show
from bokeh.io import curdoc

parser = argparse.ArgumentParser(description="Make a candlestick plot of a stock")
parser.add_argument("-s", "--stock", dest="stock", default="AMD", help="stock to plot")
parser.add_argument(
    "-c", "--count", dest="count", default="50", help="how many days of history"
)
args = parser.parse_args()

name = args.stock

stocks = parquet.read_table(
    "../data/parquet/stocks/all_stocks.parquet"
).to_pandas()[-int(args.count) :]

inc = stocks[f"{name}-Close"] > stocks[f"{name}-Open"]
dec = stocks[f"{name}-Open"] > stocks[f"{name}-Close"]

w = 12 * 60 * 60 * 1000  # half day in ms
TOOLS = "pan,wheel_zoom,box_zoom,reset,save"

curdoc().theme = "dark_minimal"
p = figure(
    x_axis_type="datetime",
    tools=TOOLS,
    sizing_mode="scale_width",
    title=f"{name} Candlestick",
)
p.xaxis.major_label_orientation = pi / 4
p.grid.grid_line_alpha = 0.3

p.segment(stocks.index, stocks[f"{name}-High"], stocks.index, stocks[f"{name}-Low"], color="white")
p.vbar(
    stocks.index[inc],
    w,
    stocks[f"{name}-Open"][inc],
    stocks[f"{name}-Close"][inc],
    fill_color="green",
    line_color="black",
)
p.vbar(
    stocks.index[dec],
    w,
    stocks[f"{name}-Open"][dec],
    stocks[f"{name}-Close"][dec],
    fill_color="red",
    line_color="black",
)

show(p)
