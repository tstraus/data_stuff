#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
import scipy.stats as stats
from pyarrow import parquet

parser = argparse.ArgumentParser(description="Calculate how correlated 2 stock prices are")
parser.add_argument("-s", "--stocks", default=["AMD", "INTC"], nargs="+")
parser.add_argument("-d", "--days", dest="days", default="365", help="Number of days in past to compare")
parser.add_argument("-f", "--field", dest="field", default="Close", help="Which data to compare. \"Open\", \"High\", \"Low\", \"Close\", \"Volume\"")
args = parser.parse_args()

first = parquet.read_table(
    f"../data/parquet/stocks/stocks/{args.stocks[0][0]}/{args.stocks[0]}.parquet"
).to_pandas()[-int(args.days) :]
second = parquet.read_table(
    f"../data/parquet/stocks/stocks/{args.stocks[1][0]}/{args.stocks[1]}.parquet"
).to_pandas()[-int(args.days) :]

correlation = stats.pearsonr(first[args.field], second[args.field])[0]

print(f"{args.stocks[0]} <-> {args.stocks[1]} = {correlation}")
