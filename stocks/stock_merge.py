#!/usr/bin/env python3

import os
from posixpath import join
import pandas
from pyarrow import parquet, Table

def read_stock(name: str, file: str):
    df = (
        parquet.read_table(file)
        .to_pandas()
        .rename(
            columns={
                "Open": f"{name}-Open",
                "High": f"{name}-High",
                "Low": f"{name}-Low",
                "Close": f"{name}-Close",
                "Adj Close": f"{name}-Adj_Close",
                "Volume": f"{name}-Volume",
            }
        )
    )

    df["Date"] = pandas.to_datetime(df["Date"])
    df.set_index("Date", inplace=True)

    return df

def read_stocks(dir: str):
    stocks = []

    for root, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith((".parquet")):
                stock_name = os.path.splitext(file)[0]
                stocks.append(read_stock(stock_name, root + "/" + file))

    return stocks

def merge(input_dir: str, output_file: str):
    merged = pandas.concat(read_stocks(input_dir), axis=1, join="outer", sort=True)
    parquet.write_table(Table.from_pandas(merged), output_file, compression="zstd", compression_level=22)

merge("../data/parquet/stocks/stocks", "../data/parquet/stocks/all_stocks.parquet")
merge("../data/parquet/stocks/etfs", "../data/parquet/stocks/all_etfs.parquet")
