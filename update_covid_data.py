#!/usr/bin/env python

import requests
from rich.progress import Progress

remote_data = [
    {
        "repo": "https://raw.githubusercontent.com/nytimes/covid-19-data/master/",
        "out": "./data/csv/covid_nytimes/",
        "files": [
            "us-states.csv"
        ]
    },
    {
        "repo": "https://raw.githubusercontent.com/datasets/covid-19/master/data/",
        "out": "./data/csv/covid/",
        "files": [
            "countries-aggregated.csv",
            "reference.csv"
        ]
    }
]

for source in remote_data:
    for file in source["files"]:
        with Progress() as progress:
            response = requests.get(source["repo"] + file, stream=True)
            total_size = len(response.content)
            t = progress.add_task(f"{file}: ", total=total_size)

            with open(source["out"] + file, "wb") as handle:
                for data in response.iter_content(1024):
                    progress.update(t, advance=len(data))
                    handle.write(data)
