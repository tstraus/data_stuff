#!/usr/bin/env python3

import make_pattern
from write_pattern import write_pattern
import dearpygui.dearpygui as dpg
import numpy as np

pattern = np.zeros([make_pattern.width, make_pattern.height])

dpg.create_context()


def update_plot():
    global pattern

    min = pattern.min()
    max = pattern.max()
    mean = pattern.mean()

    # normalize the data between 0 and 1
    normalized = (pattern - min) / (max - min)

    dpg.set_value("pattern_min", min)
    dpg.set_value("pattern_max", max)
    dpg.set_value("pattern_mean", mean)
    dpg.set_value("heatmap", [normalized])


def make_isotropic():
    global pattern

    pattern = make_pattern.make_isotropic(
        int(dpg.get_value("iso_noise_radius")),
        float(dpg.get_value("iso_noise_spread")),
        int(dpg.get_value("iso_null_height")),
        float(dpg.get_value("iso_null_gain")),
    )
    update_plot()


def make_directed():
    global pattern

    pattern = make_pattern.make_directed(
        int(dpg.get_value("dir_main_lobe_radius")),
        float(dpg.get_value("dir_main_lobe_sigma")),
        float(dpg.get_value("dir_background_average")),
        float(dpg.get_value("dir_background_spread")),
    )
    update_plot()


def write_current_pattern():
    global pattern

    write_pattern(pattern, dpg.get_value("output_filename"))


def run_ui():
    global pattern

    with dpg.window(label="Very Fake Antenna Patterns") as main_window:
        with dpg.group():
            with dpg.group(horizontal=True):
                with dpg.group():
                    # isotropic options
                    with dpg.group(horizontal=True):
                        dpg.add_text("Isotropic")
                        dpg.add_button(
                            label="Create",
                            tag="make_isotropic",
                            callback=make_isotropic,
                        )
                    dpg.add_slider_float(
                        label="Noise Radius",
                        tag="iso_noise_radius",
                        default_value=10,
                        min_value=1,
                        max_value=64,
                        width=200,
                        callback=make_isotropic,
                    )
                    dpg.add_slider_float(
                        label="Noise Spread",
                        tag="iso_noise_spread",
                        default_value=8,
                        min_value=0,
                        max_value=30,
                        width=200,
                        callback=make_isotropic,
                    )
                    dpg.add_slider_float(
                        label="Pole Null Height",
                        tag="iso_null_height",
                        default_value=35,
                        min_value=0,
                        max_value=90,
                        width=200,
                        callback=make_isotropic,
                    )
                    dpg.add_slider_float(
                        label="Pole Null Min Gain",
                        tag="iso_null_gain",
                        default_value=-30,
                        min_value=-40,
                        max_value=0,
                        width=200,
                        callback=make_isotropic,
                    )

                with dpg.group():
                    # directed options
                    with dpg.group(horizontal=True):
                        dpg.add_text("Directed")
                        dpg.add_button(
                            label="Create", tag="make_directed", callback=make_directed
                        )
                    dpg.add_slider_float(
                        label="Main Lobe Radius",
                        tag="dir_main_lobe_radius",
                        default_value=5,
                        min_value=1,
                        max_value=90,
                        width=200,
                        callback=make_directed,
                    )
                    dpg.add_slider_float(
                        label="Main Lobe Sigma",
                        tag="dir_main_lobe_sigma",
                        default_value=1,
                        min_value=0.01,
                        max_value=10,
                        width=200,
                        callback=make_directed,
                    )
                    dpg.add_slider_float(
                        label="Background Average",
                        tag="dir_background_average",
                        default_value=-30,
                        min_value=-40,
                        max_value=0,
                        width=200,
                        callback=make_directed,
                    )
                    dpg.add_slider_float(
                        label="Background Spread",
                        tag="dir_background_spread",
                        default_value=10,
                        min_value=0,
                        max_value=30,
                        width=200,
                        callback=make_directed,
                    )

                with dpg.group():
                    # pattern stats
                    with dpg.group(horizontal=True):
                        dpg.add_text("Min:")
                        dpg.add_text(tag="pattern_min", default_value=np.min(pattern))
                    with dpg.group(horizontal=True):
                        dpg.add_text("Max:")
                        dpg.add_text(tag="pattern_max", default_value=np.max(pattern))
                    with dpg.group(horizontal=True):
                        dpg.add_text("Mean:")
                        dpg.add_text(tag="pattern_mean", default_value=np.mean(pattern))

                with dpg.group():
                    # save pattern
                    dpg.add_input_text(
                        label="Output Filename",
                        tag="output_filename",
                        default_value="./pattern.csv",
                        width=-1,
                    )
                    dpg.add_button(
                        label="Save", tag="output_save", callback=write_current_pattern
                    )

            with dpg.plot(no_mouse_pos=True, height=-1, width=-1):
                dpg.bind_colormap(dpg.last_item(), dpg.mvPlotColormap_Viridis)
                dpg.add_plot_axis(
                    dpg.mvXAxis,
                    lock_min=True,
                    lock_max=True,
                    no_gridlines=True,
                    no_tick_marks=True,
                    no_tick_labels=True,
                )
                dpg.add_plot_axis(
                    dpg.mvYAxis,
                    no_gridlines=True,
                    no_tick_marks=True,
                    lock_min=True,
                    lock_max=True,
                    no_tick_labels=True,
                    tag="y_axis",
                )
                dpg.add_heat_series(
                    pattern,
                    make_pattern.height,
                    make_pattern.width,
                    scale_min=0,
                    scale_max=1,
                    tag="heatmap",
                    parent="y_axis",
                    format="",
                )

    dpg.create_viewport(title="Very Fake Antenna Patterns", width=1000, height=1000)
    dpg.set_viewport_vsync(True)
    dpg.setup_dearpygui()
    dpg.set_primary_window(main_window, True)
    dpg.show_viewport()
    dpg.start_dearpygui()

    dpg.destroy_context()


run_ui()
