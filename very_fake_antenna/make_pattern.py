import numpy as np
import scipy.fft as sf
import cv2
import time


def generate_noise(height, width, radius):
    start_time = time.perf_counter_ns()

    input = np.random.uniform(size=[height, width])
    fd = sf.fft2(input)
    fd = sf.fftshift(fd)

    # create blurred circle mask (2D low pass filter)
    mask = np.zeros_like(input)
    cy = mask.shape[0] // 2  # get center
    cx = mask.shape[1] // 2
    cv2.circle(mask, (cx, cy), radius, 1.0, -1)[0]
    mask = cv2.GaussianBlur(mask, (19, 19), 0)

    fd = np.multiply(fd, mask)
    fd = sf.ifftshift(fd)
    noise = sf.ifft2(fd).real.astype(np.float32)

    # normalize
    min = np.min(noise)
    max = np.max(noise)
    noise = (noise - min) / (max - min)

    elapsed = (time.perf_counter_ns() - start_time) / 1000000  # ns -> ms
    # print(f"time: {elapsed}ms")

    return noise


def shift(pattern, center, spread):
    output = (pattern * spread) - (0.5 * spread) + center
    # print(f"mean: {np.mean(output)}, min: {np.min(output)}, max: {np.max(output)}")
    return output


def gradient(start, stop, width, height, is_horizontal=False):
    if is_horizontal:
        return np.tile(np.linspace(start, stop, width, dtype=float), (height, 1))
    else:
        return np.tile(np.linspace(start, stop, height, dtype=float), (width, 1)).T


def add_fade(pattern, null_height, null_gain):
    fade = np.zeros_like(pattern)

    top_fade = gradient(1, 0, pattern.shape[1], null_height) * null_gain
    fade[: top_fade.shape[0], : top_fade.shape[1]] = top_fade

    bottom_fade = gradient(0, 1, pattern.shape[1], null_height) * null_gain
    fade[
        pattern.shape[0] - bottom_fade.shape[0] :,
        pattern.shape[1] - bottom_fade.shape[1] :,
    ] = bottom_fade

    return np.minimum(pattern, fade)


def add_main_lobe(pattern, radius, sigma):
    lobe = np.zeros_like(pattern)
    cy = lobe.shape[0] // 2  # get center
    cx = lobe.shape[1] // 2
    cv2.circle(lobe, (cx, cy), radius, 1.0, -1)[0]
    lobe = cv2.GaussianBlur(lobe, (19, 19), sigma) - 1
    min = np.min(pattern)
    lobe = lobe * -min

    return np.maximum(pattern, lobe)


def add_back_lobe(pattern, radius, sigma):
    min_ish = np.min(pattern / 1.5)

    right = np.zeros_like(pattern)
    cy = right.shape[0] // 2  # get center
    cx = right.shape[1] // 2
    cv2.circle(right, (right.shape[1], cy), radius, 1.0, -1)[0]
    right = cv2.GaussianBlur(right, (19, 19), sigma) - 1
    right = (right * -min_ish) + min_ish

    left = np.zeros_like(pattern)
    cy = left.shape[0] // 2  # get center
    cx = left.shape[1] // 2
    cv2.circle(left, (0, cy), radius, 1.0, -1)[0]
    left = cv2.GaussianBlur(left, (19, 19), sigma) - 1
    left = (left * -min_ish) + min_ish

    pattern = np.maximum(pattern, right)
    return np.maximum(pattern, left)


def add_side_lobe(pattern, radius, sigma, extra=0):
    thickness = int(0.4 * radius) + 8
    lobe = np.zeros_like(pattern)
    cy = lobe.shape[0] // 2  # get center
    cx = lobe.shape[1] // 2
    cv2.circle(lobe, (cx, cy), radius, 1.0, thickness)[0]
    lobe = cv2.GaussianBlur(lobe, (19, 19), sigma) - 1
    lobe = (lobe * -np.min(pattern)) + np.min(pattern / 2) + extra

    return np.maximum(pattern, lobe)


height = 181
width = 361


def make_isotropic(noise_radius, noise_spread, null_height, null_gain):
    pattern = generate_noise(height, width, noise_radius)
    pattern = shift(pattern, -(noise_spread / 2), noise_spread)

    return add_fade(pattern, null_height, null_gain)


def make_directed(
    main_lobe_radius, main_lobe_sigma, background_average, background_spread
):
    noise_radius = 10
    remaining = 90 - main_lobe_radius
    sl1_angle = int(0.3 * remaining + main_lobe_radius)
    sl2_angle = int(0.7 * remaining + main_lobe_radius)

    pattern = generate_noise(height, width, noise_radius)
    pattern = shift(pattern, background_average, background_spread)
    pattern = add_main_lobe(pattern, main_lobe_radius, main_lobe_sigma)
    pattern = add_back_lobe(pattern, 2 * main_lobe_radius + 10, 2 * main_lobe_sigma)
    pattern = add_side_lobe(pattern, sl1_angle, 2 * main_lobe_sigma)
    pattern = add_side_lobe(pattern, sl2_angle, 3 * main_lobe_sigma, -4)
    pattern = add_fade(pattern, 20, background_average - (background_spread / 2) - 5)

    return pattern

