#!/usr/bin/env python3

from make_pattern import make_isotropic, make_directed
from display_pattern import show
from write_pattern import write_pattern

pattern = make_isotropic(10, 8, 35, -30)
#pattern = make_directed(10, 2.5, -30, 10)
#pattern = make_directed(50, 50, -30, 10)
#pattern = make_directed(2, 0.5, -30, 10)
#pattern = make_directed(5, 1, -30, 10)

#write_pattern(pattern, "./pattern.csv")
show(pattern)
