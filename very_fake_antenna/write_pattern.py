import numpy as np

def write_pattern(pattern, output_file):
	temp = np.asarray(pattern)
	np.savetxt(output_file, temp, delimiter=",")
