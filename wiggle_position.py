import numpy as np
import pandas as pd
import geopandas as gp
import matplotlib.pyplot as plt

wiggle_factor = 0.02

# make individual rows for a position and quantity
df = pd.DataFrame({
	"lat": [38.255046],
	"lon": [-76.544225],
	"num": [20]
})

lats = []
lons = []
for index, row in df.iterrows():
	for _ in range(0, int(row["num"])):
		lats.append(row["lat"])
		lons.append(row["lon"])

df = pd.DataFrame({"lat": lats, "lon": lons})

# wiggle and jiggle the positions so they don't overlap
df["lat"] = df["lat"] + np.random.uniform(-wiggle_factor, wiggle_factor, len(df.index))
df["lon"] = df["lon"] + np.random.uniform(-wiggle_factor, wiggle_factor, len(df.index))

df = gp.GeoDataFrame(df, geometry=gp.points_from_xy(df.lon, df.lat), crs="EPSG:4326")

#print(df)

world = gp.read_file("./data/countries.geojson")
ax = world.plot(color="white", edgecolor="black")
df.plot(ax=ax)
plt.show()
